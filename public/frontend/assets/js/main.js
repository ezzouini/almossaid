(function(window, document, $, undefined) {
    'use strict';

    var almoussaidKey = {
        k: function(e) {
            almoussaidKey.s();
            almoussaidKey.methods();
        },
        s: function(e) {
            this._window = $(window),
                this._document = $(document),
                this._body = $('body'),
                this._html = $('html'),
                this.sideNav = $('.side-nav'),
                this._navsearch = $('.moussaid-search-area')
        },
        methods: function(e) {
            almoussaidKey.mosaidWow();
            // almoussaidKey.counterUp();
            // almoussaidKey.countDown();
            // almoussaidKey.ytPlayer();
            // almoussaidKey.splittingText();
            // almoussaidKey.tiltAnimation();
            // almoussaidKey.axilMasonary();
            almoussaidKey.scrollTop();
            // almoussaidKey.axilHover();
            // almoussaidKey.axilaccor();
            almoussaidKey.onMoveUp();
            almoussaidKey.moveUp();
            // almoussaidKey.marqueImages();
            almoussaidKey.scrollDown();
            almoussaidKey.mouseMOve();
            almoussaidKey.mouseParalax();
            almoussaidKey.stickHeader();
            almoussaidKey.mobileMenu();
            almoussaidKey.scrollSmoth();
            almoussaidKey._slickDoc();
            almoussaidKey._clickDoc();
        },

        mosaidWow: function() {
            new WOW().init();
        },

        mobileMenu: function() {
            $('.mainmenu-item > li.has-children > a').on('click', function(e) {
                e.preventDefault();
                $(this).siblings('.submenu').slideToggle('400');
                $(this).toggleClass('active').siblings('.submenu').toggleClass('is-visiable')
            })
        },

        stickHeader: function() {
            almoussaidKey._window.scroll(function() {
                if ($(this).scrollTop() > 250) {
                    $('.moussaid-header-sticky').addClass('sticky')
                } else {
                    $('.moussaid-header-sticky').removeClass('sticky')
                }
            })
        },

        mouseMOve: function() {
            $.fn.parallax = function(resistance, mouse) {
                var $el;
                $el = $(this);
                TweenLite.to($el, 0.2, {
                    x: -((mouse.clientX - window.innerWidth / 2) / resistance),
                    y: -((mouse.clientY - window.innerHeight / 2) / resistance)
                });
            };
        },

        scrollSmoth: function(e) {
            $(document).on('click', '.smoth-animation', function(event) {
                event.preventDefault();
                $('html, body').animate({
                    scrollTop: $($.attr(this, 'href')).offset().top
                }, 500);
            });
        },

        mouseParalax: function() {
            $('.paralax-area').mousemove(function(e) {
                $('.paralax--1').parallax(10, e);
                $('.paralax--2').parallax(35, e);
                $('.paralax--3').parallax(15, e);
            });
        },

        moveUp: function() {
            $(window).on('load', function() {
                $('.move-up').css('opacity', 0);
                $('.move-up').waypoint(function() {
                    $('.move-up').addClass('animate');
                }, {
                    offset: '90%'
                });
            })
        },

        marqueImages: function() {
            $('.marque-images').each(function() {
                var t = 0;
                var i = 1;
                var $this = $(this);
                setInterval(function() {
                    t += i;
                    $this.css('background-position-x', -t + 'px');
                }, 10);
            });
        },


        counterUp: function() {
            var _counter = $('.count');
            if (_counter.length) {
                _counter.counterUp({
                    delay: 10,
                    time: 1000,
                    triggerOnce: true
                });
            }
        },

        ytPlayer: function() {
            $('.play__btn').yu2fvl();
        },

        splittingText: function() {
            if (almoussaidKey._window.width() > 1199) {
                Splitting();
            }
        },

        tiltAnimation: function() {
            var _tiltAnimation = $('.paralax-image')
            if (_tiltAnimation.length) {
                _tiltAnimation.tilt({
                    max: 12,
                    speed: 1e3,
                    easing: 'cubic-bezier(.03,.98,.52,.99)',
                    transition: !1,
                    perspective: 1e3,
                    scale: 1
                })
            }
        },

        axilMasonary: function() {
            $('.moussaid-masonary-wrapper').imagesLoaded(function() {
                // filter items on button click
                $('.messonry-button').on('click', 'button', function() {
                    var filterValue = $(this).attr('data-filter');
                    $grid.isotope({
                        filter: filterValue
                    });
                });

                // init Isotope
                var $grid = $('.mesonry-list').isotope({
                    itemSelector: '.portfolio',
                    percentPosition: true,
                    transitionDuration: '0.7s',
                    layoutMode: 'fitRows',
                    masonry: {
                        // use outer width of grid-sizer for columnWidth
                        columnWidth: 1,
                    }
                });
            });

            $('.messonry-button button').on('click', function(event) {
                $(this).siblings('.is-checked').removeClass('is-checked');
                $(this).addClass('is-checked');
                event.preventDefault();
            });
        },

        countDown: function() {
            $('[data-countdown]').each(function() {
                var $this = $(this),
                    finalDate = $(this).data('countdown');
                $this.countdown(finalDate, function(event) {
                    $this.html(event.strftime('<span class="moussaid-count days"><span class="count-inner"><span class="time-count">%-D</span> <p>Days</p></span></span> <span class="moussaid-count hour"><span class="count-inner"><span class="time-count">%-H</span> <p>Hours</p></span></span> <span class="moussaid-count minutes"><span class="count-inner"><span class="time-count">%M</span> <p>Minutes</p></span></span> <span class="moussaid-count second"><span class="count-inner"><span class="time-count">%S</span> <p>Seconds</p></span></span>'));
                });
            });
        },

        scrollTop: function() {
            $.scrollUp({
                scrollText: '<span class="text">top</span>',
                easingType: 'linear',
                scrollSpeed: 900,
                animation: 'slide'
            });
        },

        onMoveUp: function() {
            almoussaidKey._window.on('load', function() {
                function allAnimation() {
                    $('.move-up').css('opacity', 0);
                    $('.move-up').waypoint(function() {
                        $('.move-up').addClass('animate');
                    }, {
                        offset: '90%'
                    });
                }
                allAnimation();
            })
        },

        axilHover: function() {
            $('.moussaid-service-area .moussaid-service , .mesonry-list .portfolio , .blog-list-wrapper .moussaid-blog, .moussaid-testimonial-single .moussaid-testimonial').mouseenter(function() {
                var self = this;
                $(self).removeClass('moussaid-control');
                setTimeout(function() {
                    $('.moussaid-service-area .active , .mesonry-list .active , .blog-list-wrapper .active,  .moussaid-testimonial-single .active').removeClass('active').addClass('moussaid-control');
                    $(self).removeClass('moussaid-control').addClass('active');
                    $('.moussaid-service.active .inner::before').css('opacity', 0.1);
                }, 0);

            });
        },

        scrollDown: function() {
            $('.smoth-animation').click(function() {
                $('html, body').animate({
                    // scrollTop: $("#sectionBottom").offset().top
                }, 1000);
            });
        },

        axilaccor: function() {
            $('.moussaid-accordion .card .card-header').siblings('.collapse.show').parent().addClass('open');
        },

        _slickDoc: function(e) {
            // Check if element exists
            $.fn.elExists = function() {
                return this.length > 0;
            };
            // Variables
            var $html = $('html'),
                $elementCarousel = $('.moussaid-carousel');

            if ($elementCarousel.elExists()) {
                var slickInstances = [];
                $elementCarousel.each(function(index, element) {
                    var $this = $(this);
                    // Carousel Options
                    var $options = typeof $this.data('slick-options') !== 'undefined' ? $this.data('slick-options') : '';
                    var $spaceBetween = $options.spaceBetween ? parseInt($options.spaceBetween) : 0,
                        $spaceBetween_xl = $options.spaceBetween_xl ? parseInt($options.spaceBetween_xl) : 0,
                        $isCustomArrow = $options.isCustomArrow ? $options.isCustomArrow : false,
                        $customPrev = $isCustomArrow === true ? ($options.customPrev ? $options.customPrev : '') : '',
                        $customNext = $isCustomArrow === true ? ($options.customNext ? $options.customNext : '') : '',
                        $vertical = $options.vertical ? $options.vertical : false,
                        $focusOnSelect = $options.focusOnSelect ? $options.focusOnSelect : false,
                        $asNavFor = $options.asNavFor ? $options.asNavFor : '',
                        $fade = $options.fade ? $options.fade : false,
                        $autoplay = $options.autoplay ? $options.autoplay : false,
                        $autoplaySpeed = $options.autoplaySpeed ? $options.autoplaySpeed : 5000,
                        $swipe = $options.swipe ? $options.swipe : false,
                        $adaptiveHeight = $options.adaptiveHeight ? $options.adaptiveHeight : false,

                        $arrows = $options.arrows ? $options.arrows : false,
                        $dots = $options.dots ? $options.dots : false,
                        $infinite = $options.infinite ? $options.infinite : false,
                        $centerMode = $options.centerMode ? $options.centerMode : false,
                        $centerPadding = $options.centerPadding ? $options.centerPadding : '',
                        $speed = $options.speed ? parseInt($options.speed) : 1000,
                        $prevArrow = $arrows === true ? ($options.prevArrow ? '<span class="' + $options.prevArrow.buttonClass + '"><i class="' + $options.prevArrow.iconClass + '"></i></span>' : '<button class="slick-prev">previous</span>') : '',
                        $nextArrow = $arrows === true ? ($options.nextArrow ? '<span class="' + $options.nextArrow.buttonClass + '"><i class="' + $options.nextArrow.iconClass + '"></i></span>' : '<button class="slick-next">next</span>') : '',
                        $slidesToShow = $options.slidesToShow ? parseInt($options.slidesToShow, 10) : 1,
                        $slidesToScroll = $options.slidesToScroll ? parseInt($options.slidesToScroll, 10) : 1;

                    /*Responsive Variable, Array & Loops*/
                    var $responsiveSetting = typeof $this.data('slick-responsive') !== 'undefined' ? $this.data('slick-responsive') : '',
                        $responsiveSettingLength = $responsiveSetting.length,
                        $responsiveArray = [];
                    for (var i = 0; i < $responsiveSettingLength; i++) {
                        $responsiveArray[i] = $responsiveSetting[i];

                    }

                    // Adding Class to instances
                    $this.addClass('slick-carousel-' + index);
                    $this.parent().find('.slick-dots').addClass('dots-' + index);
                    $this.parent().find('.slick-btn').addClass('btn-' + index);

                    if ($spaceBetween != 0) {
                        $this.addClass('slick-gutter-' + $spaceBetween);
                    }
                    if ($spaceBetween_xl != 0) {
                        $this.addClass('slick-gutter-xl-' + $spaceBetween_xl);
                    }
                    $this.slick({
                        slidesToShow: $slidesToShow,
                        slidesToScroll: $slidesToScroll,
                        asNavFor: $asNavFor,
                        autoplay: $autoplay,
                        autoplaySpeed: $autoplaySpeed,
                        speed: $speed,
                        infinite: $infinite,
                        arrows: $arrows,
                        dots: $dots,
                        vertical: $vertical,
                        focusOnSelect: $focusOnSelect,
                        centerMode: $centerMode,
                        centerPadding: $centerPadding,
                        fade: $fade,
                        adaptiveHeight: $adaptiveHeight,
                        prevArrow: $prevArrow,
                        nextArrow: $nextArrow,
                        responsive: $responsiveArray,
                    });

                    if ($isCustomArrow === true) {
                        $($customPrev).on('click', function() {
                            $this.slick('slickPrev');
                        });
                        $($customNext).on('click', function() {
                            $this.slick('slickNext');
                        });
                    }
                });

                // Updating the sliders in tab
                $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
                    $elementCarousel.slick('setPosition');
                });
            }
        },

        _clickDoc: function(e) {
            var inputblur, inputFocus, openSideNav, closeSideNav, openSubMenu, closeSubMenu, searchTriggerShow, searchTriggerHide, axilaccordion, OpenMobileMenu, closeMobileMenu, closeMenuWrapperClick, closeMobileMenu2;

            inputblur = function(e) {
                if (!$(this).val()) {
                    $(this).parent('.form-group').removeClass('focused');
                }
            };

            inputFocus = function(e) {
                $(this).parents('.form-group').addClass('focused');
            };

            openSideNav = function(e) {
                e.preventDefault();
                almoussaidKey.sideNav.addClass('opened');
                almoussaidKey._html.addClass('side-nav-opened');
            };

            closeSideNav = function(e) {
                if (!$('.side-nav, .side-nav *:not(".close-sidenav, .close-sidenav *")').is(e.target)) {
                    almoussaidKey.sideNav.removeClass('opened');
                    almoussaidKey._html.removeClass('side-nav-opened');
                }
            };

            OpenMobileMenu = function(e) {
                e.preventDefault();
                almoussaidKey._body.addClass('popup-mobile-manu-visible');
                almoussaidKey._html.css({
                    overflow: 'hidden'
                })
            };

            closeMenuWrapperClick = function(e) {
                e.target === this && almoussaidKey._body.removeClass('popup-mobile-manu-visible'),
                    almoussaidKey._html.css({
                        overflow: ''
                    });
            };

            closeMobileMenu = function(e) {
                $('.mainmenu-item > li.has-children > a').removeClass('active').siblings('.submenu').slideUp('400')
                almoussaidKey._body.removeClass('popup-mobile-manu-visible');
                almoussaidKey._html.css({
                    overflow: ''
                })
            };

            openSubMenu = function(e) {
                if (almoussaidKey._window.width() < 992) {
                    $(this).siblings('.moussaid-submenu').slideToggle('active').parents('li').toggleClass('active')
                } else {
                    $(this).siblings('.moussaid-submenu').toggleClass('visible').parents('li').toggleClass('active');
                    $(this).parents('li').siblings('.has-dropdown').removeClass('active').find('.moussaid-submenu').removeClass('visible')
                }
            };

            closeSubMenu = function(e) {
                if (!$('.mainmenu-nav ul.mainmenu li a').is(e.target)) {
                    $('.moussaid-submenu').removeClass('visible').parents('li').removeClass('active')
                }
            };

            searchTriggerShow = function(e) {
                e.preventDefault();
                almoussaidKey._navsearch.addClass('visible')
            };

            searchTriggerHide = function(e) {
                e.preventDefault();
                almoussaidKey._navsearch.removeClass('visible')
            };

            axilaccordion = function(e) {
                e.preventDefault();
                $(this).siblings('.collapse.show').parent().removeClass('open').toggleClass('active');
            }

            almoussaidKey._document
                .on('blur', 'input,textarea,select', inputblur)
                .on('focus', 'input:not([type="radio"]),input:not([type="checkbox"]),textarea,select', inputFocus)
                .on('click', '#side-nav-toggler', openSideNav)
                .on('click', '#close-sidenav , .side-nav-opened', closeSideNav)
                .on('click', '.mainmenu-nav ul.mainmenu li a', openSubMenu )
                .on('click' , closeSubMenu)
                .on('click', '.search-trigger', searchTriggerShow)
                .on('click', '.moussaid-search-area .navbar-search-close', searchTriggerHide)
                .on('click', '.moussaid-accordion .card .card-header', axilaccordion)
                .on('click', '.popup-navigation-activation', OpenMobileMenu)
                .on('click', '.close-menu', closeMobileMenu)
                .on('click', '.popup-mobile-manu', closeMenuWrapperClick)
        }
    };

    almoussaidKey.k();

})(window, document, jQuery);