<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Carbon\Carbon;

class Post extends Model
{
    //
    protected $guarded = [];

    public function user ()
    {
        return $this->belongsTo('App\User');
    }

    public function image ()
    {
        return $this->belongsTo('App\Image');
    }
    
    public function categories()
    {
        return $this->belongsToMany('App\Category');
    }

    public function tags()
    {
        return $this->belongsToMany('App\Tag','tag_post');
    }

    public function scopeActive( $query )
    {
        return $query->where(['status'=>'ACTIVE']);
    }

    public function getDescriptionAttribute()
    {
        $breaks = explode('<!-- pagebreak -->', $this->body );
        return trim( strip_tags( ( count($breaks) > 1 ? $breaks[0] : null ) ) );
    }
    public function getTimeAttribute()
    {
        return Carbon::parse($this->created_at)->isoFormat('LLLL');
    }

    public function getTimeAgoAttribute()
    {
        return Carbon::parse($this->created_at)->diffForHumans();
    }
    /*
    public function getTagsAttribute()
    {
        return explode( ',' , $this->attributes['tags'] );
    }

    public function getKeywordsAttribute()
    {
        return implode( ',' , $this->tags );
    }
    */
    
    

}
