<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\User;
use App\Post;
use App\Category;

class DashboardController extends Controller
{
    public function index(){

        $authors    = User::all()->count();
        $categories = Category::all()->count();
        $posts      = Post::all()->count();
        
        return view('admin.pages.dashboard',compact('authors','posts','categories'));
    }
}
