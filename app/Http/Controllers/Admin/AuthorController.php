<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Image as ImageModel ;
use App\User;
use Illuminate\Http\Request;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class AuthorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $authors = User::with(['image','posts'])->paginate(20);
        return view('admin.pages.author.index',compact('authors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.pages.author.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $dir = 'images/';
        if( !is_dir( storage_path( Storage::disk('public')->path($dir) ) ) ){
            Storage::disk('public')->makeDirectory($dir,0777);
        }

        $validation = $request->validate([
            'name'      => 'required',
            'username'  => 'required',
            'email'     => 'required',
            'password'  => 'required',
            'image'     => 'sometimes|image',
            'is_super'  => 'required',
            'status'    => 'required',
        ]);

        // upload image 
        if( $image = $request->file('image') ){
            
            $baseName = Str::random(10);
            $fullName = $baseName.'_author.'.$image->getClientOriginalExtension();
            
            $resize = Image::make( $image )
                ->fit(300)
                ->encode('jpg',90);
            Storage::disk('public')->put( $dir . $fullName , $resize);

            // put image info to database
            $ImageModel    = ImageModel::create([ 'path' => $dir . $fullName , 'name' => $image->getClientOriginalName() , 'mim' => $resize->mime() , 'size' => $resize->filesize() ]);
            $ImageModel->save();
            $validation['image_id'] = $ImageModel->id; 
        }
        unset($validation['image']);

        $validation['password'] = \Hash::make( $request->get('password') ); 
        //dd( $validation );
        $author = User::create($validation);
        $author->save();
        
        return redirect()->route('admin.author')->with('message','Data has been saved successfully');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $author)
    {
        //
        return view('admin.pages.author.edit',compact('author'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $author)
    {
        //

        $dir = 'images/';
        if( !is_dir( storage_path( Storage::disk('public')->path($dir) ) ) ){
            Storage::disk('public')->makeDirectory($dir,0777);
        }

        $validation = $request->validate([
            'name'      => 'required',
            'username'  => 'required',
            'email'     => 'required',
            'password'  => 'sometimes',
            'image'     => 'sometimes|image',
            'is_super'  => 'required',
            'status'    => 'required',
        ]);

        // upload image 
        if( $image = $request->file('image') ){
            
             // remove the old image
            if( $author->image ){
                File::delete( Storage::disk('public')->path('/') . $author->image->path );
                $ImageDb = ImageModel::find( $author->image->id );
                $ImageDb->delete();
            }

            $baseName = Str::random(10);
            $fullName = $baseName.'_author.'.$image->getClientOriginalExtension();
            
            $resize = Image::make( $image )
                ->fit(300)
                ->encode('jpg',90);
            Storage::disk('public')->put( $dir . $fullName , $resize);

            // put image info to database
            $ImageModel    = ImageModel::create([ 'path' => $dir . $fullName , 'name' => $image->getClientOriginalName() , 'mim' => $resize->mime() , 'size' => $resize->filesize() ]);
            $ImageModel->save();

            $author->image_id = $ImageModel->id;
        }

        
        if( $request->get('password') ){
            $author->password = \Hash::make( $request->get('password') );
        }

        $author->name     = $request->get('name');
        $author->email    = $request->get('email');
        $author->username = $request->get('username');
        $author->is_super = $request->get('is_super');
        $author->status   = $request->get('status');
        $author->save();

        return redirect()->route('admin.author')->with('message','Data has been saved successfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $author)
    {
        //

        if( $author->image ){
            File::delete( Storage::disk('public')->path('/') . $author->image->path );
            $ImageDb = ImageModel::find( $author->image->id );
            $ImageDb->delete();
        }

        $author->delete();

        return redirect()->route('admin.author')->with('message','Data has been saved successfully');
    }
}
