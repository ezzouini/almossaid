<?php

namespace App\Http\Controllers\Admin\Auth;

use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


use App\Providers\RouteServiceProvider;

class LoginController extends Controller
{
    //
    public function __construct ()
    {
        $this->middleware('guest:admin')->except('logout');
    }
    public function showLoginForm ()
    {
        return view('admin.auth.login');
    }
    public function login (Request $request)
    {
        $this->validate($request,
            [
                'email' => 'required',
                'password' => 'required',
            ]
        );

        if( Auth::guard('admin')->attempt(['email'=>$request->email , 'password'=>$request->password ],$request->remember) )
        {
            return redirect()->intended( route('admin.dashboard') );
        }
        //return redirect()->back()->with( $request->only( 'email' , 'remember' ) );
        return redirect()->back()->with( ['message'=>'Admin not found'] );

    }
    public function logout (Request $request) 
    {
        Auth::guard('admin')->logout();
        return redirect()->route('home');
    }
}
