<?php

namespace App\Http\Controllers\Control;

use App\Http\Controllers\Controller;
use App\Image as ImageModel ;
use App\User;
use Illuminate\Http\Request;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $author = Auth()->user();
        return view('control.pages.profile',compact('author'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        $author = Auth()->user();

        $dir = 'images/';
        if( !is_dir( storage_path( Storage::disk('public')->path($dir) ) ) ){
            Storage::disk('public')->makeDirectory($dir,0777);
        }

        $validation = $request->validate([
            'name'      => 'required',
            'username'  => 'required',
            'email'     => 'required',
            'password'  => 'sometimes',
            'image'     => 'sometimes|image',
            //'status'=> 'required',
        ]);

        // upload image 
        if( $image = $request->file('image') ){
            
            if( $author->image ){
                // remove the old image 
                File::delete( Storage::disk('public')->path('/') . $author->image->path );
                $ImageDb = ImageModel::find( $author->image->id );
                $ImageDb->delete();
            }

            $baseName = Str::random(10);
            $fullName = $baseName.'_author.'.$image->getClientOriginalExtension();
            
            $resize = Image::make( $image )
                ->fit(300)
                ->encode('jpg',90);
            Storage::disk('public')->put( $dir . $fullName , $resize);

            // put image info to database
            $ImageModel    = ImageModel::create([ 'path' => $dir . $fullName , 'name' => $image->getClientOriginalName() , 'mim' => $resize->mime() , 'size' => $resize->filesize() ]);
            $ImageModel->save();
            $author->image_id = $ImageModel->id;
        }

        
        $author->name = $request->get('name');
        $author->email = $request->get('email');
        $author->username = $request->get('username');
        if( $request->get('password') ){
            $author->password = \Hash::make( $request->get('password') );
        }
        $author->save();

        return view('control.pages.profile',compact('author'))->with('message','Data has been saved successfully');

    }

}
