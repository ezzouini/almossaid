<?php

namespace App\Http\Controllers\Control;

use App\Image as ImageModel ;
use App\Category;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;


class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('authorIsSuper')->except('index');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $categories = Category::with(['image','posts'])->orderBy('created_at', 'desc')->paginate(20);
        return view('control.pages.category',compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('control.pages.category');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $dir = 'images/';
        if( !is_dir( storage_path( Storage::disk('public')->path($dir) ) ) ){
            Storage::disk('public')->makeDirectory($dir,0777);
        }

        $validation = $request->validate([
            'name'  => 'required',
            'slug'  => 'sometimes',
            'image' => 'sometimes|image',
            //'status'=> 'required',
        ]);
        // upload image 
        if( $image = $request->file('image') ){
            
            $baseName = Str::random(10);
            $fullName = $baseName.'_category.'.$image->getClientOriginalExtension();
            
            $resize = Image::make( $image )
                ->fit(300,200)
                ->encode('jpg',90);
            Storage::disk('public')->put( $dir . $fullName , $resize);

            // put image info to database
            $ImageModel    = ImageModel::create([ 'path' => $dir . $fullName , 'name' => $image->getClientOriginalName() , 'mim' => $resize->mime() , 'size' => $resize->filesize() ]);
            $ImageModel->save();
            $validation['image_id'] = $ImageModel->id; 
        }
        unset($validation['image']);
        
        $category = Category::create( $validation );
        $category->save();

        return redirect()->route('control.category')->with('message','Data has been saved successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
        return view('control.pages.category',compact('category'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        //
        $dir = 'images/';
        if( !is_dir( storage_path( Storage::disk('public')->path($dir) ) ) ){
            Storage::disk('public')->makeDirectory($dir,0777);
        }

        $validation = $request->validate([
            'name'  => 'required',
            'slug'  => 'sometimes',
            'image' => 'sometimes|image',
            //'status'=> 'required',
        ]);
        // upload image 
        if( $image = $request->file('image') ){
            
            if( $category->image ){
                // remove the old image 
                File::delete( Storage::disk('public')->path('/') . $category->image->path );
                $ImageDb = ImageModel::find( $category->image->id );
                $ImageDb->delete();
            }
            
            // continue code
            $baseName = Str::random(10);
            $fullName = $baseName.'_category.'.$image->getClientOriginalExtension();
            
            $resize = Image::make( $image )
                ->fit(300,200)
                ->encode('jpg',90);
            Storage::disk('public')->put( $dir . $fullName , $resize);

            // put image info to database
            $ImageModel    = ImageModel::create([ 'path' => $dir . $fullName , 'name' => $image->getClientOriginalName() , 'mim' => $resize->mime() , 'size' => $resize->filesize() ]);
            $ImageModel->save();
            $category->image_id = $ImageModel->id;
        }
        unset($validation['image']);
        
        
        $category->name = $request->get('name');
        $category->slug = $request->get('slug');
        $category->save();

        return redirect()->route('control.category')->with('message','Data has been saved successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        // remove the image First
        if( $category->image ){
            File::delete( Storage::disk('public')->path('/') . $category->image->path );
            $ImageDb = ImageModel::find( $category->image->id );
            $ImageDb->delete();
        }

        $category->delete();
        return redirect()->route('control.category')->with('message','Data has been saved successfully');
    }
}
