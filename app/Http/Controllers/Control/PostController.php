<?php

namespace App\Http\Controllers\Control;

use App\Http\Controllers\Controller;

use App\Image as ImageModel ;
use App\Category;
use App\Post;
use App\Tag;
use Illuminate\Http\Request;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $posts = Post::with(['categories','user','image'])->where(['user_id'=> auth()->user()->id ])->orderBy('created_at', 'desc')->paginate(20);
        return view('control.pages.post',compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $categories = Category::all();
        return view('control.pages.post',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $dir = 'images/';
        if( !is_dir( storage_path( Storage::disk('public')->path($dir) ) ) ){
            Storage::disk('public')->makeDirectory($dir,0777);
        }

        // validation
        $validation = $request->validate([
            'title'  => 'required',
            'slug'   => 'required',
            'categories' => 'sometimes',
            'body'   => 'required',
            'tags'   => 'sometimes',
            'image'  => 'sometimes|image',
            'status' => 'required',
        ]);

        $categories = $validation['categories'];
        $tags       = $validation['tags'];

        unset($validation['categories']);
        unset($validation['tags']);
        $validation['user_id'] = auth()->user()->id;

        // upload image
        if( $image = $request->file('image') ){

            $baseName = Str::random(10);
            $fullName = $baseName.'_thumbnail.'.$image->getClientOriginalExtension();

            $resize = Image::make( $image )
                ->fit(800,650)
                ->encode('jpg',90);
            Storage::disk('public')->put( $dir . $fullName , $resize);

            // put image info to database
            $ImageModel    = ImageModel::create([ 'path' => $dir . $fullName , 'name' => $image->getClientOriginalName() , 'mim' => $resize->mime() , 'size' => $resize->filesize() ]);
            $ImageModel->save();
            $validation['image_id'] = $ImageModel->id;
        }
        unset($validation['image']);


        $post = Post::create($validation);
        $post->categories()->sync( $categories );

        $tagList = explode(',', $tags );
        $listTagIds = [];
        foreach( $tagList as $tag ){
            $tag = Tag::firstOrCreate(['name'=>$tag]);
            array_push($listTagIds , $tag->id );
        }
        $post->tags()->sync( $listTagIds );

        $post->save();

        return redirect()->route('control.post')->with('message','Data has been saved successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        //

        if( $post->user_id !== auth()->user()->id  ){
            return abort(403, 'Unauthorized action.');
        }
        $categories = Category::all();
        return view('control.pages.post',compact('post','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        //
        $dir = 'images/';
        if( !is_dir( storage_path( Storage::disk('public')->path($dir) ) ) ){
            Storage::disk('public')->makeDirectory($dir,0777);
        }

        // validation
        $validation = $request->validate([
            'title'  => 'required',
            'slug'   => 'required',
            'categories' => 'sometimes',
            'body'   => 'required',
            'tags'   => 'sometimes',
            'image'  => 'sometimes|image',
            'status' => 'required',
        ]);

        // upload image
        if( $image = $request->file('image') ){

            // remove the old image
            if( $post->image ){
                File::delete( Storage::disk('public')->path('/') . $post->image->path );
                $ImageDb = ImageModel::find( $post->image->id );
                $ImageDb->delete();
            }

            // continue code
            $baseName = Str::random(10);
            $fullName = $baseName.'_thumbnail.'.$image->getClientOriginalExtension();

            $resize = Image::make( $image )
                ->fit(600,450)
                ->encode('jpg',90);

            Storage::disk('public')->put( $dir . $fullName , $resize);

            // put image info to database
            $ImageModel    = ImageModel::create([ 'path' => $dir . $fullName , 'name' => $image->getClientOriginalName() , 'mim' => $resize->mime() , 'size' => $resize->filesize() ]);
            $ImageModel->save();
            $post->image_id = $ImageModel->id;
        }
        unset($validation['image']);

        $post->categories()->sync( $request->get('categories') );

        $tagList = explode(',',$request->get('tags'));
        $listTagIds = [];
        foreach( $tagList as $tag ){
            $tag = Tag::firstOrCreate(['name'=>$tag]);
            array_push($listTagIds , $tag->id );
        }
        $post->tags()->sync( $listTagIds );


        $post->title  = $request->get('title');
        $post->slug   = $request->get('slug');
        $post->body   = $request->get('body');
        $post->status = $request->get('status');
        $post->save();

        return redirect()->route('control.post')->with('message','Data has been saved successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        //
        // remove the image First
        if( $post->image ){
            File::delete( Storage::disk('public')->path('/') . $category->image->path );
            $ImageDb = ImageModel::find( $category->image->id );
            $ImageDb->delete();
        }
        $post->categories()->delete();
        $post->tags()->delete();
        $post->delete();
        return redirect()->route('control.post')->with('message','Post has been removed successfully');
    }
}
