<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Image as ImageModel ;
use Illuminate\Http\Request;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image ;

class ImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $images = ImageModel::all();
        $images->each( function ($image) {
            $image->title = $image->name;
            $image->value =  asset('storage/'. $image->path ) ;
        });
        return response($images);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $dir = 'images/';
        if( !is_dir( storage_path( Storage::disk('public')->path($dir) ) ) ){
            Storage::disk('public')->makeDirectory($dir,0777);
        }

        // Validation
        $validated = $request->validate([
            'image' => 'required|image|max:5024',
        ]);

        // upload image 
        $image = $request->file('image');
            
        $baseName = Str::random(10);
        $fullName = $baseName.'_image.'.$image->getClientOriginalExtension();
        
        $resize = Image::make( $image )
            ->encode('jpg',90);
        Storage::disk('public')->put( $dir . $fullName , $resize);

        // put image info to database
        $ImageModel    = ImageModel::create([ 'path' => $dir . $fullName , 'name' => $image->getClientOriginalName() , 'mim' => $resize->mime() , 'size' => $resize->filesize() ]);
        $ImageModel->save();
        
        return response( [ 'image' => asset('storage/'. $ImageModel->path ) ] ) ;

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ImageModel  $image
     * @return \Illuminate\Http\Response
     */
    public function show(ImageModel $image)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ImageModel  $image
     * @return \Illuminate\Http\Response
     */
    public function edit(ImageModel $image)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ImageModel  $image
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ImageModel $image)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ImageModel  $image
     * @return \Illuminate\Http\Response
     */
    public function destroy(ImageModel $image)
    {
        //
    }
}
