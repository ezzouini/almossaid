<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    //protected $redirectTo = RouteServiceProvider::HOME;
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function showLoginForm ()
    {
        return view('auth.login');
    }
    public function login (Request $request)
    {
        $this->validate($request,
            [
                'email' => 'required',
                'password' => 'required',
            ]
        );

        if( Auth::guard('web')->attempt(['email'=>$request->email , 'password'=>$request->password ],$request->remember) )
        {
            return redirect()->intended( route('control.post') );
        }
        return redirect()->back()->with( $request->only( 'email' , 'remember' ) );
    }

    public function logout (Request $request)
    {
        Auth::guard('web')->logout();
        return redirect()->route('home');
    }
}
