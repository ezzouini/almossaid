<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class AuthorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show($username)
    {
        //
        $user  = User::where(['username'=>$username])->firstOrFail();
        $posts = $user->posts()->active()->orderBy('created_at', 'desc')->paginate(20);
        return view('pages.author',compact('user','posts'));
    }

}
