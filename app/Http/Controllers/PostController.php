<?php

namespace App\Http\Controllers;

use App\post;
use Illuminate\Http\Request;
use Carbon\Carbon;


class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::with(['categories','image'])->active()->orderBy('created_at', 'desc')->paginate(20);
        return view('pages.blog.index',compact('posts'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\post  $post
     * @return \Illuminate\Http\Response
     */
    public function show_by_slug($slug)
    {
        //
        $post = Post::with('user')->where(['slug'=>$slug])->firstOrFail();
        $postSuggetion  = Post::with(['image' /*,'categories'*/])->where('id', '!=' ,$post->id )/*->whereHas('image')*/->whereHas('categories', function($q ) use ( $post ) {
            $categoriesArr = [];
            foreach( $post->categories as $category ){ array_push( $categoriesArr , $category->id ); }
            $q->whereRaw(" category_id in (".implode(",",$categoriesArr).") ");
        })->orderBy('created_at', 'desc')->offset(0)->limit(4)->get();

        $post->view = $post->view + 1 ;
        $post->save();

        return view('pages.blog.post',compact('post','postSuggetion'));
    }
}
