<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $categories = Category::where(['status'=>'ACTIVE'])->paginate(20);
        
        return view('pages.blog.category',compact('categories'));
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show_by_slug($slug)
    {
        //
        //dd( $slug );
        $category = Category::with('posts')->where(['slug'=>$slug])->firstOrFail();
        $posts    = $category->posts()->active()->orderBy('created_at', 'desc')->paginate(20);
        //dd( $category );
        return view('pages.blog.category',compact('category','posts'));
    }

}
