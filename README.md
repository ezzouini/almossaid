composer install

cp .env.example .env

php artisan key:generate

php artisan migrate

php artisan storage:link

php artisan config:clear

php artisan cache:clear

php artisan serve