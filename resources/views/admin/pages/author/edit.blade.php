@extends('admin.layouts.app')

@section('title','Update author')

@section('content')

    @if( session('message') )
        <div class="alert alert-success my-2 p-3">
            {{ session('message') }}
        </div>
    @endif

    @if( $errors->any() )
        <div class="alert alert-danger my-2 p-3">
            <ul>
                @foreach( $errors->all() as $error )
                    <li> {{ $error }} </li>
                @endforeach
            </ul>
        </div>
    @endif

    <form action="{{ route('admin.author.update',$author) }}" method="POST" enctype="multipart/form-data">
        @method('PUT')
        @csrf
        <div class="row mb-2">
            <div class="col-12">
                <input type="file" name="image" class="mb-2" id="image" accept="image/*" onchange="loadFile(event)">
                <br />
                <label for="image">
                    <img id="preview" class="img-thumbnail" src="{{ $author->image ? asset('storage/'. $author->image->path ) : asset('img/NoProfile.png') }}" width="200px"/>
                </label>
            </div>
            <div class="col-12 col-md-6">
                <div class="form-group">
                    <label for="name">Full name</label>
                    <input name="name" type="text" class="form-control" id="name" value="{{ old('name') ?? $author->name }}"  >
                </div>
            </div>
            <div class="col-12 col-md-6 ">
                <div class="form-group">
                    <label for="username">Username</label>
                    <input name="username" type="text" class="form-control" id="username" value="{{ old('username') ?? $author->username }}" >
                </div>
            </div>

            <div class="col-12 col-md-6">
                <div class="form-group">
                    <label for="email">Email address</label>
                    <input name="email" type="text" class="form-control" id="email" value="{{ old('email') ?? $author->email }}" >
                </div>
            </div>
            <div class="col-12 col-md-6">
                <div class="form-group">
                    <label for="password">Password</label>
                    <input name="password" type="password" class="form-control" id="password" >
                </div>
            </div>

            <div class="col-12 col-md-6">
                <div class="form-group">
                    <label for="status">Status </label>
                    <select name="status" class="form-control" id="status">
                        <option value="ACTIVE" {{ $author->status == 'ACTIVE' ? 'selected' : null }} > Active </option>
                        <option value="PENDING" {{ $author->status == 'PENDING' ? 'selected' : null }} > Pending </option>
                        <option value="INACTIVE" {{ $author->status == 'INACTIVE' ? 'selected' : null }} > Inactive </option>
                    </select>
                </div>
            </div>

            <div class="col-12 col-md-6">
                <div class="form-group">
                    <label for="is_super">Is super author </label>
                    <select name="is_super" class="form-control" id="is_super">
                        <option value="0" {{ !$author->is_super ? 'selected' : null }} > Simple author </option>
                        <option value="1" {{ $author->is_super ? 'selected' : null }} > Super author </option>
                    </select>
                </div>
            </div>

        </div>
        
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
    
@endsection


@section('javascript')
<script>

    var loadFile = function(event) {
        var output      = document.getElementById('preview');
        var eventTarget = event.target.files[0] ;
        if( eventTarget ){
            output.src = URL.createObjectURL( eventTarget );
            output.onload = function() {
                URL.revokeObjectURL(output.src) // free memory
            }
        }else{
            output.src = 'https://demos.creative-tim.com/vue-black-dashboard-pro/img/image_placeholder.jpg';
        }
    };
</script>
@endsection