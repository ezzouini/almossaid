@extends('admin.layouts.app')

@section('title','Authors')

@section('content')

    @if( session('message') )
        <div class="alert alert-success my-2 p-3">
            {{ session('message') }}
        </div>
    @endif

    @if( $errors->any() )
        <div class="alert alert-danger my-2 p-3">
            <ul>
                @foreach( $errors->all() as $error )
                    <li> {{ $error }} </li>
                @endforeach
            </ul>
        </div>
    @endif

    <table class="table table-striped">
        <thead class="thead-dark">
            <tr>
                <th width="150px"> <a class="btn btn-sm btn-primary" href="{{ route('admin.author.create') }}"> <i class="fas fa-plus"></i> </a> </th>
                <th width="50px" > </th>
                <th> Name </th>
                <th> username </th>
                <th> Email address </th>
                <th> Articles </th>
            </tr> 
        </thead>
        <tbody>
            @foreach( $authors as $author )
                <tr class="{{ $author->status == 'PENDING' ? 'table-warning' : null }} {{ $author->status == 'INACTIVE' ? 'table-danger' : null }}">
                    <td>
                        <a href="{{ route('author', $author->username ) }}" class="mr-2 btn btn-sm btn-primary"> <i class="fas fa-external-link-alt"></i> <a>
                        <a href="{{ route('admin.author.edit', $author ) }}" class="mr-1 btn btn-sm btn-info"> <i class="fas fa-pencil-alt"></i> <a>
                        <label class="mr-1 mt-2 btn btn-sm btn-danger"> <i class="far fa-trash-alt"></i> <label>
                        <form action="{{ route('admin.author.destroy',$author) }}" method="POST" class="d-none" >
                            @method('DELETE')
                            @csrf
                            <input type="submit" id="remove_item"  />
                        </form>
                    </td>
                    <td> @if( $author->image ) <img src="{{ asset('storage/'. $author->image->path ) }}" height="30"/> @endif </td>
                    <td> @if( $author->is_super ) <i class="fas fa-user-tie"></i> @endif {{ $author->name }} </td>
                    <td> {{ $author->username }} </td>
                    <td> {{ $author->email }} </td>
                    <td> {{ $author->posts()->count() }} </td>
                </tr>
            @endforeach
        </tbody>
    </table>

    {{ $authors->links() }}
    
@endsection
