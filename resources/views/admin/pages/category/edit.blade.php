@extends('admin.layouts.app')

@section('title','Update category')

@section('content')

    @if( session('message') )
        <div class="alert alert-success my-2 p-3">
            {{ session('message') }}
        </div>
    @endif

    @if( $errors->any() )
        <div class="alert alert-danger my-2 p-3">
            <ul>
                @foreach( $errors->all() as $error )
                    <li> {{ $error }} </li>
                @endforeach
            </ul>
        </div>
    @endif

    <form action="{{ route('admin.category.update',$category) }}" method="POST" enctype="multipart/form-data">
        @method('PUT')
        @csrf
        <div class="row mb-2">
            <div class="col-12">
                <input type="file" name="image" class="mb-2" id="image" accept="image/*" onchange="loadFile(event)">
                <br />
                <label for="image">
                    <img id="preview" class="img-thumbnail" src="{{ $category->image ? asset('storage/'. $category->image->path ) : asset('img/NoImage.png') }}" width="200px"/>
                </label>
            </div>
            <div class="col-12 col-md-6">
                <div class="form-group">
                    <label for="name">name</label>
                    <input name="name" type="text" class="form-control" id="name" value="{{ old('name') ?? $category->name }}"  >
                </div>
            </div>
            <div class="col-12 col-md-6 ">
                <div class="form-group">
                    <label for="">Category name link</label>
                    <div class="input-group mb-2">
                        <div class="input-group-prepend">
                            <div class="input-group-text"> ...category/</div>
                        </div>
                        <input name="slug" type="text" class="form-control" id="slug" value="{{ old('slug') ?? $category->slug }}" placeholder="exemple_category">
                    </div>
                </div>
            </div>

            <div class="col-12 col-md-6">
                <div class="form-group">
                    <label for="status">Status </label>
                    <select name="status" class="form-control" id="status">
                        <option value="ACTIVE" {{ $category->status == 'ACTIVE' ? 'selected' : null }} > Active </option>
                        <option value="INACTIVE" {{ $category->status == 'INACTIVE' ? 'selected' : null }} > Inactive </option>
                    </select>
                </div>
            </div>

        </div>
        
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
    
@endsection


@section('javascript')
<script>

    var loadFile = function(event) {
        var output      = document.getElementById('preview');
        var eventTarget = event.target.files[0] ;
        if( eventTarget ){
            output.src = URL.createObjectURL( eventTarget );
            output.onload = function() {
                URL.revokeObjectURL(output.src) // free memory
            }
        }else{
            output.src = 'https://demos.creative-tim.com/vue-black-dashboard-pro/img/image_placeholder.jpg';
        }
    };
</script>
@endsection