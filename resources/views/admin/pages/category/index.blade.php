@extends('admin.layouts.app')

@section('title','Categories')

@section('content')

    @if( session('message') )
        <div class="alert alert-success my-2 p-3">
            {{ session('message') }}
        </div>
    @endif

    @if( $errors->any() )
        <div class="alert alert-danger my-2 p-3">
            <ul>
                @foreach( $errors->all() as $error )
                    <li> {{ $error }} </li>
                @endforeach
            </ul>
        </div>
    @endif

    <table class="table table-striped">
        <thead class="thead-dark">
            <tr>
                <th width="150px"> <a class="btn btn-sm btn-primary" href="{{ route('admin.category.create') }}"> <i class="fas fa-plus"></i> </a> </th>
                <th width="50px" > </th>
                <th> Name </th>
                <th> Articles </th>
            </tr> 
        </thead>
        <tbody>
            @foreach( $categories as $category )
                <tr class="{{ $category->status == 'PENDING' ? 'table-warning' : null }} {{ $category->status == 'INACTIVE' ? 'table-danger' : null }}">
                    <td>
                        <a href="{{ route('category', $category->slug ) }}" class="mr-2 btn btn-sm btn-primary"> <i class="fas fa-external-link-alt"></i> <a>
                        <a href="{{ route('admin.category.edit', $category ) }}" class="mr-1 btn btn-sm btn-info"> <i class="fas fa-pencil-alt"></i> <a>
                        <label class="mr-1 mt-2 btn btn-sm btn-danger"> <i class="far fa-trash-alt"></i> <label>
                        <form action="{{ route('admin.category.destroy',$category) }}" method="POST" class="d-none" >
                            @method('DELETE')
                            @csrf
                            <input type="submit" id="remove_item"  />
                        </form>
                    </td>
                    <td> @if( $category->image ) <img src="{{ asset('storage/'. $category->image->path ) }}" height="30"/> @endif </td>
                    <td> {{ $category->name }} </td>
                    <td> {{ $category->posts()->count() }} </td>
                </tr>
            @endforeach
        </tbody>
    </table>

    {{ $categories->links() }}
    
@endsection
