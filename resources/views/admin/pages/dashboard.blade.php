@extends('admin.layouts.app')

@section('title','Dashboard')

@section('content')

    @if( session('message') )
        <div class="alert alert-success my-2 p-3">
            {{ session('message') }}
        </div>
    @endif

    @if( $errors->any() )
        <div class="alert alert-danger my-2 p-3">
            <ul>
                @foreach( $errors->all() as $error )
                    <li> {{ $error }} </li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="row">
        <div class="col col-md-4 mb-2">
            <div class="card bg-info text-light shadow">
                <div class="card-header"> <b> Posts </b> <span class="float-right"><a href="#" class="text-light"><i class="far fa-eye"></i></a><span> </div>
                <div class="card-body"> <h2>{{ $posts }}</h2> </div>
            </div>
        </div>

        <div class="col col-md-4 mb-2">
            <div class="card bg-success text-light shadow">
                <div class="card-header"> <b> Categories </b> <span class="float-right"><a href="#" class="text-light"><i class="far fa-eye"></i></a><span> </div>
                <div class="card-body"> <h2>{{ $categories }}</h2> </div>
            </div>
        </div>

        <div class="col col-md-4 mb-2">
            <div class="card bg-danger text-light shadow">
                <div class="card-header"> <b> Authors </b> <span class="float-right"><a href="#" class="text-light"><i class="far fa-eye"></i></a><span> </div>
                <div class="card-body"> <h2>{{ $authors }}</h2> </div>
            </div>
        </div>

    </div>
    
@endsection
