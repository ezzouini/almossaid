@extends('layouts.app')

@section('title', 'Mobile Development Services' )

@section('content')

<div class="moussaid-breadcrumb-area breadcrumb-style-2 single-service pt--170 pb--70 theme-gradient2">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6 order-2 order-lg-1 mt_md--30 mt_sm--30">
                <div class="inner">
                    <ul class="moussaid-breadcrumb liststyle d-flex">
                        <li class="moussaid-breadcrumb-item"><a href="/">Home</a></li>
                        <li class="moussaid-breadcrumb-item active" aria-current="page">Dedicated Software Development Team</li>
                    </ul>
                    <h1 class="title">Dedicated Software Development Team
                    </h1>
                    <p>
                        A dedicated development team at Almossaid is a contract engagement model built around businesses that need to quickly scale up their internal design and development team or augment their resources with a special talent.
                    </p>
                </div>
            </div>
            <div class="col-lg-6 order-1 order-lg-2">
                <div class="thumbnail text-left text-lg-right">
                    <div class=" text-right">
                        <img class="" src="{{asset('frontend/assets/images/shape/s2.svg')}}" width="80%"
                            alt="Slider images">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="moussaid-service-area ax-section-gap bg-color-lightest">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="section-title text-left mb--40">
                    <h2 class="title wow" data-splitting>Dedicated software development team services
                    </h2>
                    <p> Almoussaid offers the dedicated software development team service for those of our customers who want complete control of the development process, as well as for those who want to outsource the project management to us.</p>
                </div>
            </div>
        </div>
        <div class="row">

            <div class="col-lg-6 col-md-6 col-12 mt--10 mt_md--40 mt_sm--30">
                <div class="almoussaid-service2 text-left moussaid-control paralax-image">
                    <div class="inner">
                        <div class="icon">
                            <img src="{{asset('frontend/assets/images/icons/reducing-costs.svg')}}" alt="Icon Images">

                        </div>
                        <div class="content">
                            <h4 class="title">Reducing software development costs</h4>
                            <p>

                                Eliminate overheads with an external team.<br>

                                The in-house project team requires extra expenses that include rent, infrastructure,
                                insurance, bonuses, and more. We’ll manage your overheads while you focus on your
                                product’s success.

                            </p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-6 col-md-6 col-12 mt--10 mt_md--40 mt_sm--30">
                <div class="almoussaid-service2 text-left moussaid-control paralax-image">
                    <div class="inner">
                        <div class="icon">
                            <img src="{{asset('frontend/assets/images/icons/specific-expertise.svg')}}" alt="Icon Images">

                        </div>
                        <div class="content">
                            <h4 class="title"> Gaining specific expertise</h4>
                            <p>

                                Only qualified specialists to join your team.<br>
                                If you need to leverage your in-house resources with specific expertise – frontend,
                                backend development, QA, or UX/UI design, we are ready to apply the best practices
                                around major domains.

                            </p>



                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-6 col-md-6 col-12 mt--10 mt_md--40 mt_sm--30">
                <div class="almoussaid-service2 text-left moussaid-control paralax-image">
                    <div class="inner">
                        <div class="icon">
                            <img src="{{asset('frontend/assets/images/icons/controlling-project.svg')}}" alt="Icon Images">

                        </div>
                        <div class="content">
                            <h4 class="title">Controlling your project deliverables</h4>
                            <p>

                                Agile approach to controlling your project.<br>
                                After you start working with our team, you can be sure they’ll be available as long as you need them, from phase 0 to the support stage, maximizing your project’s profits, as needed.                            </p>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
@include('/layouts/partials/call')

@endsection
