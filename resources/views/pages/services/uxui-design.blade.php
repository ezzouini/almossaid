@extends('layouts.app')

@section('title', 'UX/UI Design Services' )

@section('content')



<div class="moussaid-breadcrumb-area breadcrumb-style-2 single-service pt--170 pb--70 theme-gradient2">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6 order-2 order-lg-1 mt_md--30 mt_sm--30">
                <div class="inner">
                    <ul class="moussaid-breadcrumb liststyle d-flex">
                        <li class="moussaid-breadcrumb-item"><a href="/">Home</a></li>
                        <li class="moussaid-breadcrumb-item active" aria-current="page">UX/UI Design Services</li>
                    </ul>
                    <h1 class="title">UX/UI Design Services
                    </h1>
                    <p>We help businesses by delivering expert user experience consulting, research, and design
                        services to create new products or improve existing ones. Our user-centered design
                        vision helps us solve customer problems and spirit up your brand, your product, your
                        services.

                    </p>
                </div>
            </div>
            <div class="col-lg-6 order-1 order-lg-2">
                <div class="thumbnail text-left text-lg-right">
                    <div class=" text-right">
                        <img class="" src="{{asset('frontend/assets/images/shape/s2.svg')}}" width="80%"
                            alt="Slider images">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="moussaid-service-area ax-section-gap bg-color-lightest">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="section-title mb--40 text-left">
                    <h2 class="title wow" data-splitting>UX/UI Design Services We Provide
                    </h2>
                    <p>Research => Design => Improve
                    </p>
                </div>
            </div>
        </div>
        <div class="row">

            <div class="col-lg-6 col-md-6 col-12 mt--10 mt_md--40 mt_sm--30">
                <div class="almoussaid-service2 text-left moussaid-control paralax-image">
                    <div class="inner">
                        <div class="icon">
                            <img src="{{asset('frontend/assets/images/icons/ux-ui-design.svg')}}" alt="Icon Images">

                        </div>
                        <div class="content">
                            <h4 class="title">UX/UI Design</h4>
                            <p>
                                Create a product that users will love.<br>
                                We design user interfaces that impress your customers and solve their
                                problems.<br>
                                Create a product that users will love.
                                <ul class="liststyle bullet-list">
                                    <li>Information Architecture</li>
                                    <li>Wireframes Design</li>
                                    <li>Visual UI Design</li>
                                    <li>Clickable Prototypes</li>
                                    <li>Front Dev </li>
                                </ul>

                            </p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-6 col-md-6 col-12 mt--10 mt_md--40 mt_sm--30">
                <div class="almoussaid-service2 text-left moussaid-control paralax-image">
                    <div class="inner">
                        <div class="icon">
                            <img src="{{asset('frontend/assets/images/icons/ux-consulting.svg')}}" alt="Icon Images">

                        </div>
                        <div class="content">
                            <h4 class="title">UX Consulting</h4>
                            <p>
                                Explore all the possible solutions.<br>
                                Our consulting can help you discover the weak points and opportunities for your
                                product.<br>

                                <ul class="liststyle bullet-list">
                                    <li>UX Audit</li>
                                    <li>UX Workshops</li>
                                    <li>UX Research</li>
                                    <li>Persona Mapping</li>
                                    <li> Scenario Mapping</li>
                                    <li>Usabillity Testing</li>
                                </ul>

                            </p>
                        </div>
                    </div>
                </div>
            </div>



        </div>
    </div>
</div>

@include('/layouts/partials/call')



@endsection