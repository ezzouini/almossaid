@extends('layouts.app')

@section('title', 'Mobile Development Services' )

@section('content')

<div class="moussaid-breadcrumb-area breadcrumb-style-2 single-service pt--170 pb--70 theme-gradient2">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6 order-2 order-lg-1 mt_md--30 mt_sm--30">
                <div class="inner">
                    <ul class="moussaid-breadcrumb liststyle d-flex">
                        <li class="moussaid-breadcrumb-item"><a href="/">Home</a></li>
                        <li class="moussaid-breadcrumb-item active" aria-current="page">Mobile DevelopmentServices</li>
                    </ul>
                    <h1 class="title">Mobile Development
                    </h1>
                    <p>Almossaid develops mobile apps for both Android and iOS – powerful, easy to use and
                        always carrying a unique design.
                    </p>
                </div>
            </div>
            <div class="col-lg-6 order-1 order-lg-2">
                <div class="thumbnail text-left text-lg-right">
                    <div class=" text-right">
                        <img class="" src="{{asset('frontend/assets/images/shape/s2.svg')}}" width="80%"
                            alt="Slider images">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="moussaid-service-area ax-section-gap bg-color-lightest">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="section-title  mb--40 text-left">
                    <h2 class="title wow" data-splitting>Mobile App Structure</h2>
                    <p> If you expect your app to be user-friendly, intuitive, and beautiful at the same time -
                        you’re in the right place. Get your web application done with extreme care of
                        functionality and visual quality.</p>
                </div>
            </div>
        </div>
        <div class="row">

            <div class="col-lg-6 col-md-6 col-12 mt--10 mt_md--40 mt_sm--30">
                <div class="almoussaid-service2 text-left moussaid-control paralax-image">
                    <div class="inner">
                        <div class="icon">
                            <img src="{{asset('frontend/assets/images/icons/client.svg')}}" alt="Icon Images">

                        </div>
                        <div class="content">
                            <h4 class="title">Client</h4>
                            <p>

                                The clean and fast client side of an app<br />

                                We develop all the activities and events that take place <br />within the app
                            </p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-6 col-md-6 col-12 mt--10 mt_md--40 mt_sm--30">
                <div class="almoussaid-service2 text-left moussaid-control paralax-image">
                    <div class="inner">
                        <div class="icon">
                            <img src="{{asset('frontend/assets/images/icons/backend.svg')}}" alt="Icon Images">

                        </div>
                        <div class="content">
                            <h4 class="title">Backend</h4>
                            <p>
                                Sound and secure server side of a web app<br>
                                Our mobile backend development team takes care of server and business logic.
                            </p>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-lg-6 col-md-6 col-12 mt--10 mt_md--40 mt_sm--30">
                <div class="almoussaid-service2 text-left moussaid-control paralax-image">
                    <div class="inner">
                        <div class="icon">
                            <img src="{{asset('frontend/assets/images/icons/database.svg')}}" alt="Icon Images">

                        </div>
                        <div class="content">
                            <h4 class="title">Database</h4>
                            <p>

                                Reliable storage for the relevant data<br>
                                We make sure that the database for your mobile app is lightweight and secure.
                            </p>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-lg-6 col-md-6 col-12 mt--10 mt_md--40 mt_sm--30">
                <div class="almoussaid-service2 text-left moussaid-control paralax-image">
                    <div class="inner">
                        <div class="icon">
                            <img src="{{asset('frontend/assets/images/icons/hosting.svg')}}" alt="Icon Images">
                        </div>
                        <div class="content">
                            <h4 class="title">Hosting</h4>
                            <p>
                                Flexible and scalable cloud hosting<br>
                                The modern cloud services allow us to build a scalable mobile app architecture.
                            </p>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
@include('/layouts/partials/call')

@endsection
