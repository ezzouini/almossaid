@extends('layouts.app')

@section('title', 'Web Development Services' )

@section('content')


<div class="moussaid-breadcrumb-area breadcrumb-style-2 single-service pt--170 pb--70 theme-gradient2">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6 order-2 order-lg-1 mt_md--30 mt_sm--30">
                <div class="inner">
                    <ul class="moussaid-breadcrumb liststyle d-flex">
                        <li class="moussaid-breadcrumb-item"><a href="/">Home</a></li>
                        <li class="moussaid-breadcrumb-item active" aria-current="page">Web Development Services</li>
                    </ul>
                    <h1 class="title">Web Development Services
                    </h1>
                    <p>We know how to reach your business goals with beautiful, fast and secure web
                        applications. We deliver custom web development services using Laravel and NodeJs on the
                        backend and leverage React and VueJs on the frontend.
                    </p>
                </div>
            </div>
            <div class="col-lg-6 order-1 order-lg-2">
                <div class="thumbnail text-left text-lg-right">
                    <div class=" text-right">
                        <img class="" src="{{asset('frontend/assets/images/shape/s2.svg')}}" width="80%"
                            alt="Slider images">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="moussaid-service-area ax-section-gap bg-color-lightest">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="section-title  mb--40 text-left">
                    <h2 class="title wow" data-splitting>Web App Structure and Stack</h2>
                    <p>We provide full-cycle of product development, from consulting and planning to web
                        design/development and QA. We also deliver any of the web development services
                        separately, as needed.</p>
                </div>
            </div>
        </div>
        <div class="row">

            <div class="col-lg-6 col-md-6 col-12 mt--10 mt_md--40 mt_sm--30">
                <div class="almoussaid-service2 text-left moussaid-control paralax-image">
                    <div class="inner">
                        <div class="icon">
                            <img src="{{asset('frontend/assets/images/icons/frontend.svg')}}" alt="Icon Images">

                        </div>
                        <div class="content">
                            <h4 class="title">Frontend</h4>
                            <p>
                                Clean and fast client side of a web app<br />
                                We leverage modern frameworks and libraries to develop the client side of the
                                application represented by the user interface.</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-6 col-md-6 col-12 mt--10 mt_md--40 mt_sm--30">
                <div class="almoussaid-service2 text-left moussaid-control paralax-image">
                    <div class="inner">
                        <div class="icon">
                            <img src="{{asset('frontend/assets/images/icons/backend.svg')}}" alt="Icon Images">

                        </div>
                        <div class="content">
                            <h4 class="title">Backend</h4>
                            <p>

                                Sound and secure server side of a web app<br>

                                We use Laravel and Node.js on the backend to make the server
                                side of the application that embodies the product’s business logic.</p>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-lg-6 col-md-6 col-12 mt--10 mt_md--40 mt_sm--30">
                <div class="almoussaid-service2 text-left moussaid-control paralax-image">
                    <div class="inner">
                        <div class="icon">
                            <img src="{{asset('frontend/assets/images/icons/database.svg')}}" alt="Icon Images">

                        </div>
                        <div class="content">
                            <h4 class="title">Database</h4>
                            <p>

                                Reliable storage for the relevant data<br>

                                By choosing the right database, we make sure that our web applications are able
                                to store data reliably and securely.</p>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-lg-6 col-md-6 col-12 mt--10 mt_md--40 mt_sm--30">
                <div class="almoussaid-service2 text-left moussaid-control paralax-image">
                    <div class="inner">
                        <div class="icon">
                            <img src="{{asset('frontend/assets/images/icons/hosting.svg')}}" alt="Icon Images">
                        </div>
                        <div class="content">
                            <h4 class="title">Hosting</h4>
                            <p>
                                Flexible and scalable cloud hosting<br>
                                We install and manage your server infrastructure and deploy your application
                                code in the cloud.
                            </p>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

@include('/layouts/partials/call')


@endsection