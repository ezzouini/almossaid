@extends('layouts.app')

@section('title', 'Blog' )


@section('content')

<div class="moussaid-breadcrumb-area breadcrumb-style-2 single-service pt--170 pb--70 theme-gradient2">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6 order-2 order-lg-1 mt_md--30 mt_sm--30">
                <div class="inner">
                    <ul class="moussaid-breadcrumb liststyle d-flex">
                        <li class="moussaid-breadcrumb-item"><a href="/">Home</a></li>
                        <li class="moussaid-breadcrumb-item active" aria-current="page">Blog</li>
                    </ul>
                    <h1 class="title">Blog</h1>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="moussaid-blog-area ax-section-gap bg-color-white">
    <div class="container">
        <div class="row ">
            <div class="col-lg-12">
                <div class="row justify-content-center">
                    @foreach( $category as $cat )
                    <div class="col col-md-2">
                        <div class="card-category">
                            <a href="{{ route('category', $cat->slug ) }}">
                                <h5 class="card-title"> {{ $cat->name }} {{ $cat->posts()->count() ? '('.$cat->posts()->count().')' : null }}</h5>
                            </a>
                        </div>
                    </div>
                    @endforeach

                </div>
            </div>
        </div>
        <div class="row blog-list-wrapper mt--20">

            @foreach ($posts as $post)
                <div class="col-lg-6 col-md-6 col-12">
                    <div class="moussaid-blog moussaid-control mt--40 active move-up wow {{ !$post->image ? "bg-dark text-light" : null }}"
                        itemprop="blogPost" itemscope="itemscope" itemtype="http://schema.org/BlogPosting">
                        <meta itemtype="https://schema.org/WebPage" itemid="{{ $post->id }}" itemprop="mainEntityOfPage"
                            itemscope="itemscope">
                        <meta
                            content="{{ \Carbon\Carbon::parse( $post->created_at , 'Europe/London' )->format('Y-m-d\TH:i:s.uP') }}"
                            itemprop="datePublished dateModified">

                        @if( $post->image )
                        <meta content="{{ asset('storage/'. $post->image->path ) }}" itemprop="url">
                        <div class="thumbnail">
                            <div class="image">
                                <img src="{{ asset('storage/'. $post->image->path ) }}" class="card-img-top" alt="...">
                            </div>
                        </div>

                        @else
                        <div class="thumbnail">
                            <div class="image">
                                <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT3TUtNnemqQRksgLh7VKvCBy0aGeQcQMxT_g&usqp=CAU" class="card-img-top" alt="...">
                            </div>
                        </div>
                        @endif

                        <div class="content">
                            <div class="content-wrap paralax-image">
                                <div class="inner">
                                    @foreach( $post->categories as $category )
                                    <a href="{{ route('category', ( $category->slug ?? $category ) ) }}"><span
                                            class="category">{{ $category->name }}</span></a>
                                    @endforeach

                                    <h5 class="title"><a href="{{ route('post', $post->slug ) }}">{{ $post->title }}</a>
                                    </h5>
                                    <p> {!! substr(strip_tags($post->description), 0, 100) !!}...</p>
                                    <small>{{ $post->timeAgo }}</small>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach

            {{ $posts->links() }}

        </div>
    </div>

</div>

@endsection