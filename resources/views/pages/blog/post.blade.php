@extends('layouts.app')

@section('title', $post->title )

@section('meta_tags')
    @include('meta::manager', [
        'title'         => $post->title,
        'description'   => $post->description ,
        'image'         => $post->image ? asset('storage/'. $post->image->path ) : null ,
        'author'        => $post->user ? $post->user->name : null ,
        //'keywords'      => $post->keywords
    ])
@endsection

@section('content')


<div class="moussaid-breadcrumb-area breadcrumb-style-2 pt--170 pb--70 theme-gradient2">
    <div class="container">
        <div class="row align-items-center ">
            <div class="col-lg-12 order-2 order-lg-1 mt_md--30 mt_sm--30">
                <div class="inner ">
                    <ul class="moussaid-breadcrumb liststyle d-flex">
                        <li class="moussaid-breadcrumb-item"><a href="/">Home</a></li>
                        <li class="moussaid-breadcrumb-item active" aria-current="page">Blog Details</li>
                    </ul>
                    <h1 class="title">
                        {{ $post->title }}
                    </h1>
                </div>
            </div>
        </div>
    </div>
</div>



<div class="moussaid-blog-area ax-section-gap bg-color-white">
    <div class="container">
        <div class="row row--40">
            <div class="col-lg-8 col-md-12 col-12">
                <div class="moussaid-blog-details-area">
                    <div class="wrapper">
                        <div class="blog-top">
                            <h3 class="title"><h1 itemprop="name" > {{ $post->title }} </h1></h3>
                            <div class="author">
                                @if( $post->user && $post->user->image  )
                                    <div class="author-thumb">
                                        <img src="{{ asset('storage/'. $post->user->image->path ) }}" alt="Blog Author">
                                    </div>
                                @endif
                                <div class="info">
                                    @if( $post->user )    
                                        <meta content="{{ route('author',$post->user->username ) }}" itemprop="url">
                                        <h6>Author : <a href="{{ route('author',$post->user->username) }}"><span itemprop="name">{{ '@'. $post->user->username }}</span></a></h6>
                                    @endif
                                    <ul class="blog-meta">
                                        <li>
                                            {{ $post->time }}</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="thumbnail mb--60 mb_sm--20 mb_md--20">
                            @if( $post->image )
                            <div itemprop="image" itemscope="itemscope" itemtype="https://schema.org/ImageObject">
                                <meta content="{{ asset('storage/'. $post->image->path ) }}" itemprop="url">
                                <div class="my-5">
                                    <img src="{{ asset('storage/'. $post->image->path ) }}"  class="w-100"  alt="...">
                                </div>
                            </div>
                        @endif
                        </div>
                        <div class="content mb--40 mb_sm--20 mb_md--20">
                            {!! $post->body !!}
                        </div>
                       
                      
                        <div class="blog-share d-flex flex-wrap align-items-center mb--80 mb_sm--30 mb">
                            <span class="text">Share on:</span>
                            <ul class="social-share d-flex">
                                <li><a href="https://www.facebook.com/sharer/sharer.php?u={{ route('post', $post->slug ) }}"><i class="fab fa-facebook-f"></i>Facebook</a></li>
                                <li><a href="https://twitter.com/intent/tweet?url={{ route('post', $post->slug ) }}&text={{ $post->title }}"><i class="fab fa-twitter"></i>twitter</a></li>
                                <li><a href="https://www.linkedin.com/shareArticle?mini=true&url={{ route('post', $post->slug ) }}&title={{ $post->title }}&summary={{ $post->slug }}&source="><i class="fab fa-linkedin-in"></i>Linkedin</a></li>
                            </ul>
                        </div>

                        @if( $post->user && $post->user->image  )
                        <div class="moussaid-blog-author">
                            <div class="author d-flex">
                                @if( $post->user->image  )
                                <div class="author-thumb">
                                    <img  src="{{ asset('storage/'. $post->user->image->path ) }}" alt="{{ $post->user->name }}">
                                </div>
                                @endif
                                <div class="info">
                                    <h5 class="title"><a href="#"> {{ $post->user->name }} </a></h5>
                                    <p class="subtitle-2"> {{ $post->user->biography }} </p>
                                    <ul class="social-share justify-content-start">
                                        @if( $post->user->website_link  ) <li><a href="{{ $post->user->website_link }}"   target="_blank"> <i class="fas fa-globe"></i> </a></li>      @endif
                                        @if( $post->user->facebook_link ) <li><a href="{{ $post->user->facebook_link }}"  target="_blank"> <i class="fab fa-facebook-f"></i> </a></li> @endif
                                        @if( $post->user->twitter_link  ) <li><a href="{{ $post->user->twitter_link }}"   target="_blank"> <i class="fab fa-twitter"></i> </a></li> @endif
                                        @if( $post->user->linkedin_link ) <li><a href="{{ $post->user->linkedin_link }}"  target="_blank"> <i class="fab fa-linkedin-in"></i> </a></li> @endif
                                        @if( $post->user->instagram_link) <li><a href="{{ $post->user->instagram_link }}" target="_blank"> <i class="fab fa-instagram"></i> </a></li> @endif
                                        @if( $post->user->behance_link  ) <li><a href="{{ $post->user->behance_link }}"   target="_blank"> <i class="fab fa-behance"></i> </a></li> @endif
                                        @if( $post->user->github_link   ) <li><a href="{{ $post->user->github_link }}"    target="_blank"> <i class="fab fa-github"></i> </a></li> @endif
                                    </ul>
                                </div>
                            </div>
                        </div>
                        @endif
                    </div>
                   

                   

                </div>
            </div>
            <div class="col-lg-4 col-md-12 col-12 mt_md--40 mt_sm--40">
                <!-- Start Blog Sidebar  -->
                <div class="moussaid-blog-sidebar">

                    <!-- Start Single Widget  -->
                    <div class="moussaid-single-widget search">
                        <h4 class="title mb--30">Search</h4>
                        <div class="inner">
                            <form action="#" class="blog-search">
                                <input type="text" placeholder="Search…">
                                <button class="search-button"><i class="fal fa-search"></i></button>
                            </form>
                        </div>
                    </div>
                    <!-- End Single Widget  -->

                    <!-- Start Single Widget  -->
                    <div class="moussaid-single-widget category mt--80 mt_sm--30 mt_md--30 mt_lg--40">
                        <h4 class="title mb--30">Categories</h4>
                        <div class="inner">
                            <ul class="category-list">
                                @foreach( $category as $cat )
                                        <li><a href="{{ route('category', $cat->slug ) }}">{{ $cat->name }}
                                            {{ $cat->posts()->count() ? '('.$cat->posts()->count().')' : null }}</a></li>
                                @endforeach
                               
                            </ul>
                        </div>
                    </div>
                    <!-- End Single Widget  -->

                    <!-- Start Single Widget  -->
                    <div class="moussaid-single-widget share mt--80 mt_sm--30 mt_md--30 mt_lg--40">
                        <div class="inner">
                            <div class="blog-share d-flex flex-wrap">
                                <span>Follow:</span>
                                <ul class="social-list d-flex">
                                    <li><a target="_BLANK" href="https://www.facebook.com/almossaidweb"><i class="fab fa-facebook-f"></i></a></li>
                                    <li><a target="_BLANK" href="https://www.instagram.com/almossaidweb/"><i class="fab fa-instagram"></i></a></li>
                                    <li><a target="_BLANK" href="https://www.linkedin.com/company/almossaid"><i class="fab fa-linkedin-in"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="moussaid-single-widget small-post-wrapper mt--80 mt_sm--30 mt_md--30 mt_lg--40">
                        <h4 class="title mb--30">Suggested articles</h4>
                        <div class="inner">
                            @foreach ($postSuggetion as $post)
                                <div class="small-post">
                                    <div class="thumbnail">
                                        <a href="">
                                            <img src="{{ asset('storage/'. $post->image->path ) }}" alt="Blog Image">
                                        </a>
                                    </div>
                                    <div class="content">
                                        <h6>
                                            <a href="{{ route('post', $post->slug ) }}">{{ $post->title }}</a>
                                            </h6>
                                        <ul class="blog-meta">
                                            <li>{{ $post->timeAgo }}</li>
                                        </ul>
                                    </div>
                                </div>
                            @endforeach
                            
                        
                        </div>
                    </div>
                    @if( $post->tags )
                    <div class="moussaid-single-widget tags mt--80 mt_sm--30 mt_md--30 mt_lg--40">
                        <h4 class="title mb--30">Tags</h4>
                        <div class="inner">
                            <ul class="tags-list">
                                @foreach( $post->tags as $tag )
                                <li><a href="#"> {{ $tag }} </a></li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    @endif
                    
                </div>
            </div>
        </div>
    </div>
</div>

@include('/layouts/partials/call')



@endsection
