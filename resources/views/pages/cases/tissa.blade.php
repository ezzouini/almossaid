@extends('layouts.app')

@section('title', 'Tisa app' )

@section('content')

<div class="moussaid-breadcrumb-area breadcrumb-style-2 single-service pt--170 pb--70 theme-gradient2">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6 order-2 order-lg-1 mt_md--30 mt_sm--30">
                <div class="inner">
                    <ul class="moussaid-breadcrumb liststyle d-flex">
                        <li class="moussaid-breadcrumb-item"><a href="/">Home</a></li>
                        <li class="moussaid-breadcrumb-item active" aria-current="page">Tissa App</li>
                    </ul>
                    <h1 class="title">Tissa App
                    </h1>
                    <p>
                        Delivery service
                    </p>
                </div>
            </div>
            <div class="col-lg-6 order-1 order-lg-2">
                <div class="thumbnail text-left text-lg-right">
                    <div class=" text-right">
                        <img class="" src="{{asset('frontend/assets/images/shape/s2.svg')}}" width="80%"
                            alt="Slider images">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="moussaid-project-brief project-bief-styles order-style-2 ax-section-gap bg-color-lightest">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6 col-xl-5 col-md-12 col-12 order-2 order-lg-1 mt_md--30 mt_sm--30">
                <div class="content">
                    <div class="inner move-up wow">
                        <h2 class="title mb--20">Project brief</h2>
                        <p class="mb--30">We are building the 'Tisa app' to make delivery service easy between the
                            restaurant, the customer and the delivery man, and we are opening new delivery job
                            opportunities</p>
                            <a class="moussaid-button btn-large btn-transparent" href="https://tisaapp.com">
                                <span class="button-text "> Visit the website </span>
                                <span class="button-icon"></span>
                            </a>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-xl-6 offset-xl-1 col-md-12 col-12 order-1 order-lg-2">
                <div class="thumbnail position-relative">
                    <img class="image w-100 paralax-image"
                        src="{{asset('frontend/assets/images/portfolio/tisa8.png')}}" alt="Featured Images">
                </div>
            </div>
        </div>
    </div>

</div>

<div class="moussaid-project-solutions-area shape-group-position ax-section-gap bg-color-white">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 offset-lg-2">
                <div class="content">
                    <div class="section-title-inner text-left">
                        <h2 class="title mb--20 move-up wow">problems</h2>
                        <p class="subtitle-2 mb--30 move-up wow">
                            1- If you need an order, you have to make a call to the delivery company, and this takes
                            time to search for the price of the order and determine the sufficient period for delivery.
                            that thing cost money for make the call and take long time between you and the delivery
                            support<br />
                            2 - The delivery service cannot filter the real customer from the non-real ....<br />
                            3 - The delivery service cannot accommodate more than 5 orders at the same time, it is very
                            difficult and the bills cannot be saved for each month or year, so this matter is very
                            complicated with the passage of time<br />
                            4 - The delivery service cannot synchronize orders with the cyclists closest to the
                            restaurants to fulfill the order in the shortest time possible <br />

                        </p>
                        <h2 class="title mb--20 move-up wow">solutions</h2>

                        <p class="subtitle-2 mb--30 move-up wow">


                            1 - build an application who has foods by restaurant and categories can everyone select the
                            specific order<br />
                            2 - the client can recommend something in favorite<br />
                            3 - the client can order something by date and time<br />

                            4 - the delivery company can see the orders real time and easy to manage between them
                        </p>
                    </div>
                    <div class="thumbnail mt--60 move-up wow">
                        <img class="w-100 paralax-image"
                            src="{{asset('frontend/assets/images/portfolio/tisa8.png')}}" alt="Featured Images">
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

{{--
<div class="moussaid-website">
    <div class="thumbnail text-center">
        <img class="light-image w-100" src="{{asset('frontend/assets/images/portfolio/tisa4.png')}}" alt="Featured Images">
        <img class="dark-image w-100" src="{{asset('frontend/assets/images/portfolio/tisa6.png')}}" alt="Featured Images">
    </div>
</div>
--}}





@include('/layouts/partials/call')

@endsection
