@extends('layouts.app')

@section('title', 'home' )


@section('content')


@include('/layouts/partials/slide')
@include('/layouts/partials/services')
@include('/layouts/partials/expertise')
@include('/layouts/partials/cases')
{{-- @include('/layouts/partials/call') --}}
{{-- @include('/layouts/partials/latestposts') --}}



<div class="moussaid-service-area ax-section-gap backgound-dots">
    <div class="container">
        <div class="row">
            <div class="col-lg-9">
                <div class="section-title text-left pb--45">
                    <span class="sub-title extra08-color wow" data-splitting>Technologies</span>
                    <h2 class="title wow" data-splitting><span>Technologies We Use</span></h2>
                    <p>
                        Decide on a relevant technology stack. Almossaid professionals will help you choose the
                        right technologies for your software development project.
                    </p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col col-sm-4 col-lg-2 col-xl-2">
                <div class="technology_item">
                    <img src="{{asset('frontend/assets/images/technologies/figma.svg')}}" alt="figma" />
                    <h6>Figma</h6>
                </div>
            </div>
            <div class="col col-sm-4 col-lg-2 col-xl-2">
                <div class="technology_item">
                    <img src="{{asset('frontend/assets/images/technologies/photoshop.svg')}}" alt="Photoshop" />
                    <h6>Adobe photoshop</h6>
                </div>
            </div>
            <div class="col col-sm-4 col-lg-2 col-xl-2">
                <div class="technology_item">
                    <img src="{{asset('frontend/assets/images/technologies/illustrator.svg')}}" alt="illustrator" />
                    <h6>Adobe illustrator</h6>
                </div>
            </div>
            <div class="col col-sm-4 col-lg-2 col-xl-2">
                <div class="technology_item">
                    <img src="{{asset('frontend/assets/images/technologies/xd.svg')}}" alt="xd" />
                    <h6>Adobe XD</h6>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col col-sm-4 col-lg-2 col-xl-2">
                <div class="technology_item">
                    <img src="{{asset('frontend/assets/images/technologies/reactjs.svg')}}" alt="reactjs" />
                    <h6>React js</h6>
                </div>
            </div>
            <div class="col col-sm-4 col-lg-2 col-xl-2">
                <div class="technology_item">
                    <img src="{{asset('frontend/assets/images/technologies/vuejs.svg')}}" alt="vuejs" />
                    <h6>Vue js</h6>
                </div>
            </div>
            <div class="col col-sm-4 col-lg-2 col-xl-2">
                <div class="technology_item">
                    <img src="{{asset('frontend/assets/images/technologies/nextjs.svg')}}" alt="nextjs" />
                    <h6>Next js</h6>
                </div>
            </div>
            <div class="col col-sm-4 col-lg-2 col-xl-2">
                <div class="technology_item">
                    <img src="{{asset('frontend/assets/images/technologies/wordpress.svg')}}" alt="wordpress" />
                    <h6>Wordpress</h6>
                </div>
            </div>
            <div class="col col-sm-4 col-lg-2 col-xl-2">
                <div class="technology_item">
                    <img src="{{asset('frontend/assets/images/technologies/js.svg')}}" alt="javascript" />
                    <h6>Javascript</h6>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col col-sm-4 col-lg-2 col-xl-2">
                <div class="technology_item">
                    <img src="{{asset('frontend/assets/images/technologies/nodejs.svg')}}" alt="nodejs" />
                    <h6>Node js</h6>
                </div>
            </div>
            <div class="col col-sm-4 col-lg-2 col-xl-2">
                <div class="technology_item">
                    <img src="{{asset('frontend/assets/images/technologies/laravel.svg')}}" alt="laravel" />
                    <h6>Laravel</h6>
                </div>
            </div>
            <div class="col col-sm-4 col-lg-2 col-xl-2">
                <div class="technology_item">
                    <img src="{{asset('frontend/assets/images/technologies/php.svg')}}" alt="php" />
                    <h6>Php</h6>
                </div>
            </div>
            <div class="col col-sm-4 col-lg-2 col-xl-2">
                <div class="technology_item">
                    <img src="{{asset('frontend/assets/images/technologies/mysql.svg')}}" alt="mysql" />
                    <h6>MySql</h6>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col col-sm-4 col-lg-2 col-xl-2">
                <div class="technology_item">
                    <img src="{{asset('frontend/assets/images/technologies/reactjs.svg')}}" alt="reactjs" />
                    <h6>React native</h6>
                </div>
            </div>
            <div class="col col-sm-4 col-lg-2 col-xl-2">
                <div class="technology_item">
                    <img src="{{asset('frontend/assets/images/technologies/pwa.svg')}}" alt="pwa" />
                    <h6>Progressive Web Apps</h6>
                </div>
            </div>
            <div class="col col-sm-4 col-lg-2 col-xl-2">
                <div class="technology_item">
                    <img src="{{asset('frontend/assets/images/technologies/fluter.svg')}}" alt="fluter" />
                    <h6>Flutter</h6>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="moussaid-team-area shape-position ax-section-gap ">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6 col-xl-6">
                <div class="thumbnail">
                    <div class="image text-center">
                        <img src="{{asset('frontend/assets/images/team/undraw_team_collaboration_re_ow29.svg')}}" alt="Team Images" style="max-height:200px" >
                    </div>
                </div>
            </div>
            <div class="col-lg-5 col-xl-5 offset-xl-1 mt_md--40 mt_sm--40">
                <div class="content">
                    <div class="inner">
                        <div class="section-title text-left">
                            <span class="sub-title extra08-color wow" data-splitting>our team</span>
                            <h2 class="title wow" data-splitting>Meet Our Management Team</h2>
                            <p class="subtitle-2 wow pr--0" data-splitting>Hire developers that approach
                                projects not as simple technology tasks, but as business problems in need of a
                                solution. Decrease time-to-market with expert teams that will design, build, and
                                deploy your vision.</p>
                            <div class="moussaid-button-group mt--40">
                                <a class="moussaid-button btn-large btn-transparent" href="/company"><span
                                        class="button-text">Our Team</span><span class="button-icon"></span></a>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="shape-group">
        <div class="shape shape-1 customOne">
            <i class="icon icon-shape-06"></i>
        </div>


    </div>
</div>





@endsection
