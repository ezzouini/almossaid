@extends('layouts.app')

@section('title', 'Contact us' )

@section('content')

<div class="moussaid-breadcrumb-area breadcrumb-style-default pt--170 pb--70 theme-gradient2">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="inner">
                    <ul class="moussaid-breadcrumb liststyle d-flex">
                        <li class="moussaid-breadcrumb-item"><a href="/">Home</a></li>
                        <li class="moussaid-breadcrumb-item active" aria-current="page">Contact</li>
                    </ul>
                    <h1 class="moussaid-page-title">Contact</h1>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="moussaid-contact-area moussaid-shape-position ax-section-gap bg-color-white">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-12 col-xl-5 col-12 d-none">
                <div class="contact-form-wrapper ">
                    <div class="moussaid-contact-form contact-form-style-1 ">
                        <div class="d-none">
                        <h3 class="title">Let's Talk</h3>

                        <form id="contact-form" method="POST" action="mail.php">
                            <div class="form-group">
                                <input name="con_name" placeholder="Name" type="text">
                                <span class="focus-border"></span>
                            </div>
                            <div class="form-group">
                                <input name="con_email" placeholder="Email" type="email">
                                <span class="focus-border"></span>
                            </div>
                            <div class="form-group">
                                <input type="text" placeholder="Phone">
                                <span class="focus-border"></span>
                            </div>
                            <div class="form-group">
                                <input type="text" placeholder="Subject">
                                <span class="focus-border"></span>
                            </div>
                            <div class="form-group">
                                <textarea name="con_message" placeholder="Your message"></textarea>
                                <span class="focus-border"></span>
                            </div>

                            <div class="form-group">
                                <input type="submit" value="Send message">
                                <p class="form-messege"></p>
                            </div>
                        </form>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-12 mt_md--40 mt_sm--40">
                <div class="moussaid-address-wrapper">
                    <div class="row">
                        <div class="col-12 col-md-6 ">
                            <div class="moussaid-address wow move-up">
                                <div class="inner">
                                    <div class="icon">
                                        <i class="fas fa-phone"></i>
                                    </div>
                                    <div class="content">
                                        <h4 class="title">Phone</h4>
                                        <p>Our customer care is open from Mon-Fri, 9:00 am to 6:00 pm</p>
                                        <p><a class="moussaid-link" href="tel:+212670039289">+2126 70 03 9289</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 ">
                            <div class="moussaid-address wow move-up ">
                                <div class="inner">
                                    <div class="icon">
                                        <i class="fas fa-envelope"></i>
                                    </div>
                                    <div class="content">
                                        <h4 class="title">Email</h4>
                                        <p>Our support team will get back to in 48-h during standard business hours.
                                        </p>
                                        <p><a class="moussaid-link" href="mailto:contact@almossaid.com"> contact@almossaid.com </a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
</div>


@endsection
