@extends('layouts.app')

@section('title', $post->title )

@section('meta_tags')
    @include('meta::manager', [
        'title'         => $post->title,
        'description'   => $post->description ,
        'image'         => $post->image ? asset('storage/'. $post->image->path ) : null ,
        'author'        => $post->user ? $post->user->name : null ,
        'keywords'      => $post->tags
    ])
@endsection

@section('content')
<div class="container">
    <div itemprop="blogPost" itemscope="itemscope" itemtype="http://schema.org/BlogPosting">
        <meta itemtype="https://schema.org/WebPage" itemid="{{ $post->id }}" itemprop="mainEntityOfPage" itemscope="itemscope">
        <meta content="{{ route('post', $post->slug ) }}" itemprop="url">
        <meta content="{{ $post->description }}" itemprop="description">

        <h1 itemprop="name" > {{ $post->title }} </h1>
        
        @if( $post->categories )
            @foreach( $post->categories as $category )
                <a href="{{ route('category', ( $category->slug ?? $category ) ) }}">{{ $category->name }}</a>
            @endforeach
            |
        @endif
        <a href="{{ route('post', $post->slug ) }}" rel="bookmark" itemprop="datePublished dateModified" title="{{ \Carbon\Carbon::parse( $post->created_at , 'Europe/London' )->format('Y-m-d\TH:i:s.uP') }}">
            {{ $post->time }}
        </a>
        
        @if( $post->user )
            <p class="float-lg-right" itemprop="author" itemscope="itemscope" itemtype="http://schema.org/Person">
                <meta content="{{ route('author',$post->user->username ) }}" itemprop="url">
                <span >Author : <a href="{{ route('author',$post->user->username) }}"><span itemprop="name">{{ '@'. $post->user->username }}</span></a></span>
            </p>
        @endif
        
        @if( $post->image )
            <div itemprop="image" itemscope="itemscope" itemtype="https://schema.org/ImageObject">
                <meta content="{{ asset('storage/'. $post->image->path ) }}" itemprop="url">
                
                <div class="my-5">
                    <img src="{{ asset('storage/'. $post->image->path ) }}" class="img-thumbnail shadow-lg " width="100%" alt="...">
                </div>
            </div>
        @endif
        <div class="row" >
            <div class="col-2 text-center"  >
                <div class="sticky-top" style="top:200px; z-index:0;"  >
                    <a target="_BLANK" href="https://www.facebook.com/sharer/sharer.php?u={{ route('post', $post->slug ) }}" class="shadow btn btn-primary w-50 m-2 px-1 "><i class="fab fa-facebook-f"></i></a>
                    <a target="_BLANK" href="https://www.linkedin.com/shareArticle?mini=true&url={{ route('post', $post->slug ) }}&summary={{ $post->title }}" class="shadow btn btn-primary w-50 m-2 px-1 "><i class="fab fa-linkedin-in"></i></a>
                    <a target="_BLANK" href="https://twitter.com/intent/tweet?url={{ route('post', $post->slug ) }}&text={{ $post->title }}" class="shadow btn btn-primary w-50 m-2 px-1 "><i class="fab fa-twitter"></i></a>
                    <a target="_BLANK" href="https://wa.me/?text={{ route('post', $post->slug ) }}" class="shadow btn btn-primary w-50 m-2 px-1 "><i class="fab fa-whatsapp"></i></a>
                </div>
            </div>
            <div class="col-10" itemprop="description articleBody">
                {!! $post->body !!}
            </div>
        </div>
    </div>

    <hr class="my-5" />

    <div class="row row-cols-1 row-cols-md-4">
        @foreach( $postSuggetion as $post )
        <div class="col mb-4">
            <div class="card h-100 shadow {{ !$post->image ? "bg-dark text-light" : null }}">
                @if( $post->image )
                <a href="{{ route('post', $post->slug ) }}">
                    <img src="{{ asset('storage/'. $post->image->path ) }}" class="card-img-top" alt="...">
                </a>
                @endif
                <div class="card-body">
                    <a href="{{ route('post', $post->slug ) }}"><h6 class="card-title"> {{ $post->title }} </h6></a>
                    
                </div>
                <div class="card-footer">
                    @foreach( $post->categories as $category )
                        <a href="{{ route('category', ( $category->slug ?? $category ) ) }}">{{ $category->name }}</a>
                    @endforeach
                    |
                    {{ $post->timeAgo }}
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>
@endsection
