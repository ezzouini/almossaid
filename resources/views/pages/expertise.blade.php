@extends('layouts.app')

@section('title', 'Software Development Expertise' )

@section('content')

<div class="moussaid-breadcrumb-area breadcrumb-style-2 single-service pt--170 pb--70 theme-gradient2">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6 order-2 order-lg-1 mt_md--30 mt_sm--30">
                <div class="inner">
                    <ul class="moussaid-breadcrumb liststyle d-flex">
                        <li class="moussaid-breadcrumb-item"><a href="/">Home</a></li>
                        <li class="moussaid-breadcrumb-item active" aria-current="page">Expertise</li>
                    </ul>
                    <h1 class="title">Almossaid Software Development Expertise
                    </h1>
                    <p>
                        We offer full-stack mobile and web development services together with requirements analysis and project management. We have all the expertise you need to produce a fully fledged, stable, and scalable product.
                    </p>
                </div>
            </div>
            <div class="col-lg-6 order-1 order-lg-2">
                <div class="thumbnail text-left text-lg-right">
                    <div class=" text-right">
                        <img class="" src="{{asset('frontend/assets/images/shape/s2.svg')}}" width="80%"
                            alt="Slider images">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="moussaid-service-area ax-section-gap ">
    <div class="container">
        <div class="row">
            <div class="col-lg-7">
                <div class="section-title text-left pb--45">
                    <h2 class="title wow" data-splitting><span>Our Expertise
                    </span></h2>
                </div>
            </div>
        </div>
        <div class="row">

            <div class="col-lg-3 col-md-6 col-sm-6 col-12 move-up wow ">
                <div class="almoussaid-service text-center moussaid-control paralax-image">
                    <div class="inner">
                        <div class="icon">
                            <img src="{{asset('frontend/assets/images/icons/e-commerce.svg')}}"
                            alt="Icon Images">
                        </div>
                        <div class="content">
                            <h4 class="title">E-Commerce</h4>
                            <p>Online stores, retail marketplaces and ecommerce startups.</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-6 col-sm-6 col-12 move-up wow">
                <div class="almoussaid-service text-center moussaid-control paralax-image">
                    <div class="inner">
                        <div class="icon">
                            <img src="{{asset('frontend/assets/images/icons/e-learning.svg')}}"
                            alt="Icon Images">
                        </div>
                        <div class="content">
                            <h4 class="title">E-learning</h4>
                            <p>Online training, learning management systems, and mobile apps.</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-6 col-sm-6 col-12 move-up wow">
                <div class="almoussaid-service text-center moussaid-control paralax-image">
                    <div class="inner">
                        <div class="icon">
                            <img src="{{asset('frontend/assets/images/icons/travel.svg')}}" alt="Icon Images">
                        </div>
                        <div class="content">
                            <h4 class="title">Travel & Hospitality</h4>
                            <p>Solutions for tour operators, travel & hospitality agencies and corporate
                                clients.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-12 move-up wow">
                <div class="almoussaid-service text-center moussaid-control paralax-image">
                    <div class="inner">
                        <div class="icon">
                            <img src="{{asset('frontend/assets/images/icons/indusrty.svg')}}" alt="Icon Images">
                        </div>
                        <div class="content">
                            <h4 class="title">Industry</h4>
                            <p>Outsourcing software development services to beat off market competition
                            </p>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
</div>
@include('/layouts/partials/call')

@endsection
