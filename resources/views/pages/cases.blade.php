@extends('layouts.app')

@section('title', 'Case Studies' )

@section('content')

<div class="moussaid-breadcrumb-area breadcrumb-style-2 single-service pt--170 pb--70 theme-gradient2">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6 order-2 order-lg-1 mt_md--30 mt_sm--30">
                <div class="inner">
                    <ul class="moussaid-breadcrumb liststyle d-flex">
                        <li class="moussaid-breadcrumb-item"><a href="/">Home</a></li>
                        <li class="moussaid-breadcrumb-item active" aria-current="page">Cases</li>
                    </ul>
                    <h1 class="title">Case Studies
                    </h1>
                </div>
            </div>
            <div class="col-lg-6 order-1 order-lg-2">
                <div class="thumbnail text-left text-lg-right">
                    <div class=" text-right">
                        <img class="" src="{{asset('frontend/assets/images/shape/s2.svg')}}" width="80%"
                            alt="Slider images">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="moussaid-service-area ax-section-gap ">
    <div class="container">
        <div class="row">
            <div class="col-lg-7">
                <div class="section-title text-left pb--45">
                    <span class="sub-title extra08-color wow" data-splitting>Case studies</span>
                    <h2 class="title wow" data-splitting><span>Our Cases</span></h2>
                </div>
            </div>
        </div>
        <div class="row">

            <div class="col-lg-12 col-12 move-up wow ">
                <div class="almoussaid-cases text-center moussaid-control ">
                    <div class="almoussaid-cases-left text-left">
                        <div class="section-title text-left">
                            <span class="sub-title primary-color wow animated" style="visibility: visible;">Web
                                Application E-Commerce 🛍️</span>
                            <h2 class="title wow animated" style="visibility: visible;">ELECTROMENAGER</h2>
                            <p class="subtitle-2 wow animated" style="visibility: visible;">
                                HTML/CSS3, VueJs, Laravel, MySQL
                            </p>
                            <a class="moussaid-button btn-large btn-transparent" href="https://alloelectromenager.com/"><span
                                    class="button-text">Visiting site</span><span class="button-icon"></span></a>
                        </div>
                    </div>

                    <div class="almoussaid-cases-right text-left">
                        <img src="{{asset('frontend/assets/images/portfolio/case1.png')}}" alt="" />
                    </div>
                </div>
            </div>

            <div class="col-lg-12 col-12 move-up wow ">
                <div class="almoussaid-cases text-center moussaid-control ">
                    <div class="almoussaid-cases-left text-left">
                        <div class="section-title text-left">
                            <span class="sub-title primary-color wow animated" style="visibility: visible;">
                                Mobile App 🛍️</span>
                            <h2 class="title wow animated" style="visibility: visible;">Tisa - Mobile App</h2>
                            <p class="subtitle-2 wow animated" style="visibility: visible;">
                                React Native - Node js</p>
                            <a class="moussaid-button btn-large btn-transparent" href="{{ route('cases.tisaapp') }}">
                                <span class="button-text">Project</span>
                                <span class="button-icon"></span>
                            </a>

                            <a class="moussaid-button btn-large btn-transparent" href="//tisaapp.com">
                                <span class="button-text">Visiting site</span>
                                <span class="button-icon"></span>
                            </a>
                        </div>
                    </div>

                    <div class="almoussaid-cases-right text-left">
                        <img src="{{asset('frontend/assets/images/portfolio/case2.png')}}" alt="" />
                    </div>
                </div>
            </div>

            <div class="col-lg-12 col-12 move-up wow ">
                <div class="almoussaid-cases text-center moussaid-control ">
                    <div class="almoussaid-cases-left text-left">
                        <div class="section-title text-left">
                            <span class="sub-title primary-color wow animated" style="visibility: visible;">Web
                                Application E-Commerce 🛍️</span>
                            <h2 class="title wow animated" style="visibility: visible;">Energy Market - e-Commerce </h2>
                            <p class="subtitle-2 wow animated" style="visibility: visible;">
                                React js - laravel</p>
                            <a class="moussaid-button btn-large btn-transparent" href="http://energymarket.ma/"><span
                                    class="button-text">Visiting site</span><span class="button-icon"></span></a>
                        </div>
                    </div>

                    <div class="almoussaid-cases-right text-left">
                        <img src="{{asset('frontend/assets/images/portfolio/energy.png')}}" alt="" />
                    </div>
                </div>
            </div>

            <div class="col-lg-12 col-12 move-up wow ">
                <div class="almoussaid-cases text-center moussaid-control ">
                    <div class="almoussaid-cases-left text-left">
                        <div class="section-title text-left">
                            <span class="sub-title primary-color wow animated" style="visibility: visible;">Web Application Real Estate Platform 🏢</span>
                            <h2 class="title wow animated" style="visibility: visible;">Immo101 - Real Estate Platform                       </h2>
                            <p class="subtitle-2 wow animated" style="visibility: visible;">
                                HTML/CSS3, JavaScript, Ajax, Laravel, MySql</p>
                            <a class="moussaid-button btn-large btn-transparent" href="https://immo101.ma/"><span
                                    class="button-text">Visiting site</span><span class="button-icon"></span></a>
                        </div>
                    </div>

                    <div class="almoussaid-cases-right text-left">
                        <img src="{{asset('frontend/assets/images/portfolio/immo.png')}}" alt="" />
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-12 move-up wow ">
                <div class="almoussaid-cases text-center moussaid-control ">
                    <div class="almoussaid-cases-left text-left">
                        <div class="section-title text-left">
                            <span class="sub-title primary-color wow animated" style="visibility: visible;">Web Application Hotel 🏨</span>
                            <h2 class="title wow animated" style="visibility: visible;">Riad Milouda - Platforme Hotel                     </h2>
                            <p class="subtitle-2 wow animated" style="visibility: visible;">
                                HTML/CSS3, JavaScript, Ajax, Laravel, MySql</p>
                            <a class="moussaid-button btn-large btn-transparent" href="https://riad-milouda.com/"><span
                                    class="button-text">Visiting site</span><span class="button-icon"></span></a>
                        </div>
                    </div>

                    <div class="almoussaid-cases-right text-left">
                        <img src="{{asset('frontend/assets/images/portfolio/riad.png')}}" alt="" />
                    </div>
                </div>
            </div>

            <div class="col-lg-12 col-12 move-up wow ">
                <div class="almoussaid-cases text-center moussaid-control ">
                    <div class="almoussaid-cases-left text-left">
                        <div class="section-title text-left">
                            <span class="sub-title primary-color wow animated" style="visibility: visible;">Web Application Catalogue 🛍️
                                </span>
                            <h2 class="title wow animated" style="visibility: visible;">El Ouasti - Catalogue                   </h2>
                            <p class="subtitle-2 wow animated" style="visibility: visible;">
                                HTML/CSS3, JavaScript, Ajax, Laravel, MySql</p>
                            <a class="moussaid-button btn-large btn-transparent" href="http://elouasti.com/public/"><span
                                    class="button-text">Visiting site</span><span class="button-icon"></span></a>
                        </div>
                    </div>

                    <div class="almoussaid-cases-right text-left">
                        <img src="{{asset('frontend/assets/images/portfolio/ousti.png')}}" alt="" />
                    </div>
                </div>
            </div>

            <div class="col-lg-12 col-12 move-up wow ">
                <div class="almoussaid-cases text-center moussaid-control ">
                    <div class="almoussaid-cases-left text-left">
                        <div class="section-title text-left">
                            <span class="sub-title primary-color wow animated" style="visibility: visible;">Web Application Language school 🏫
                                </span>
                            <h2 class="title wow animated" style="visibility: visible;">
                                RMT-Ruhr - Platforme Ecole                  </h2>
                            <p class="subtitle-2 wow animated" style="visibility: visible;">
                                HTML/CSS3, JavaScript, Ajax, Laravel, MySql</p>
                            <a class="moussaid-button btn-large btn-transparent" href="https://rmt-ruhr.de/"><span
                                    class="button-text">Visiting site</span><span class="button-icon"></span></a>
                        </div>
                    </div>

                    <div class="almoussaid-cases-right text-left">
                        <img src="{{asset('frontend/assets/images/portfolio/rmt.png')}}" alt="" />
                    </div>
                </div>
            </div>

            <div class="col-lg-12 col-12 move-up wow ">
                <div class="almoussaid-cases text-center moussaid-control ">
                    <div class="almoussaid-cases-left text-left">
                        <div class="section-title text-left">
                            <span class="sub-title primary-color wow animated" style="visibility: visible;">Web Application Creche 🏫
                                </span>
                            <h2 class="title wow animated" style="visibility: visible;">

                            Institution Bakrim - Platforme Ecole                  </h2>
                            <p class="subtitle-2 wow animated" style="visibility: visible;">
                                HTML/CSS3, JavaScript, Ajax, PHP, MySql</p>
                            <a class="moussaid-button btn-large btn-transparent" href="http://institution-bakrim.com/"><span
                                    class="button-text">Visiting site</span><span class="button-icon"></span></a>
                        </div>
                    </div>

                    <div class="almoussaid-cases-right text-left">
                        <img src="{{asset('frontend/assets/images/portfolio/bakrim.png')}}" alt="" />
                    </div>
                </div>
            </div>


            <div class="col-lg-12 col-12 move-up wow ">
                <div class="almoussaid-cases text-center moussaid-control ">
                    <div class="almoussaid-cases-left text-left">
                        <div class="section-title text-left">
                            <span class="sub-title primary-color wow animated" style="visibility: visible;">Web Design 🖌

                                </span>
                            <h2 class="title wow animated" style="visibility: visible;">

                                Hypnoses Bienveillantes                 </h2>
                            <p class="subtitle-2 wow animated" style="visibility: visible;">
                                HTML/CSS3, JavaScript, WordPress</p>
                            <a class="moussaid-button btn-large btn-transparent" href="https://hypnoses-bienveillantes.fr/"><span
                                    class="button-text">Visiting site</span><span class="button-icon"></span></a>
                        </div>
                    </div>

                    <div class="almoussaid-cases-right text-left">
                        <img src="{{asset('frontend/assets/images/portfolio/hypnose.png')}}" alt="" />
                    </div>
                </div>
            </div>

            <div class="col-lg-12 col-12 move-up wow ">
                <div class="almoussaid-cases text-center moussaid-control ">
                    <div class="almoussaid-cases-left text-left">
                        <div class="section-title text-left">
                            <span class="sub-title primary-color wow animated" style="visibility: visible;">Web Portfolio 🛍️ </span>
                            <h2 class="title wow animated" style="visibility: visible;">
                                Mica on earth
                            </h2>
                            <p class="subtitle-2 wow animated" style="visibility: visible;">
                                HTML/CSS3, Bootstrap , Jquery
                            </p>
                            <a class="moussaid-button btn-large btn-transparent" href="https://micaonearth.com/">
                                <span class="button-text">Visiting site</span>
                                <span class="button-icon"></span>
                            </a>
                        </div>
                    </div>

                    <div class="almoussaid-cases-right text-left">
                        <img src="{{asset('frontend/assets/images/portfolio/micaonearth.png')}}" alt="" />
                    </div>
                </div>
            </div>

        </div>
    </div>

</div>
@include('/layouts/partials/call')



@endsection
