@extends('layouts.app')

@section('title', 'Categories' )

@section('content')
<div class="container">
    <div class="row row-cols-1 row-cols-md-4">
        @foreach( $categories as $category )
        <div class="col mb-4">
            <div class="card bg-light h-100 shadow">
                @if( $category->image )
                <a href="{{ route('category', $category->slug ) }}">
                    <img src="{{ asset('storage/'. $category->image->path ) }}" class="card-img" alt="..." style="opacity: .2;">
                </a>
                @endif
                <div class="card-img-overlay text-center">
                    <a href="{{ route('category', $category->slug ) }}"><h5 class="card-title"> {{ $category->name }} </h5></a>
                    <p> {{ $category->posts()->count() ? $category->posts()->count().' Posts' : null }}  </p>
                </div>
            </div>
        </div>
        @endforeach
    </div>
    {{ $categories->links() }}
</div>
@endsection
