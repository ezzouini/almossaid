@extends('layouts.app')

@section('title', 'home' )

@section('content')
<div class="container">
    <!-- Carousel And new feeds + Selection categories -->
    <div class="row">
    </div>
    <!-- Posts and paginations -->
    <div class="row row-cols-1 row-cols-md-3">
        @foreach( $posts as $post )
        <div class="col mb-4">
            <div class="card h-100 shadow {{ !$post->image ? "bg-dark text-light" : null }}" itemprop="blogPost" itemscope="itemscope" itemtype="http://schema.org/BlogPosting" >
                <meta itemtype="https://schema.org/WebPage" itemid="{{ $post->id }}" itemprop="mainEntityOfPage" itemscope="itemscope">
                <meta content="{{ \Carbon\Carbon::parse( $post->created_at , 'Europe/London' )->format('Y-m-d\TH:i:s.uP') }}" itemprop="datePublished dateModified"  >
                
                @if( $post->image )
                <meta content="{{ asset('storage/'. $post->image->path ) }}" itemprop="url">
                <a href="{{ route('post', $post->slug ) }}">
                    <img src="{{ asset('storage/'. $post->image->path ) }}" class="card-img-top" alt="...">
                </a>
                @endif
                <div class="card-body">
                    <a href="{{ route('post', $post->slug ) }}"><h5 class="card-title" itemprop="name headline"> {{ $post->title }}  </h5></a>
                    @if( $post->description )
                        <p class="card-text" itemprop="description articleBody"> {{ $post->description }} </p>
                    @endif
                </div>
                <div class="card-footer">
                    @foreach( $post->categories as $category )
                        <a href="{{ route('category', ( $category->slug ?? $category ) ) }}">{{ $category->name }}</a>
                    @endforeach
                    |
                    {{ $post->timeAgo }}
                </div>
            </div>
        </div>
        @endforeach
    </div>
    {{ $posts->links() }}
</div>
@endsection