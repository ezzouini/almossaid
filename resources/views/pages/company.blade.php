@extends('layouts.app')

@section('title', 'About us' )

@section('content')



<div class="moussaid-breadcrumb-area breadcrumb-style-2 single-service pt--170 pb--70 theme-gradient2">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6 order-2 order-lg-1 mt_md--30 mt_sm--30">
                <div class="inner">
                    <ul class="moussaid-breadcrumb liststyle d-flex">
                        <li class="moussaid-breadcrumb-item"><a href="/">Home</a></li>
                        <li class="moussaid-breadcrumb-item active" aria-current="page">Company</li>
                    </ul>
                    <h1 class="title">Company</h1>
                    <p>Almossaid is a full-stack software development company with extensive experience in
                        UX/UI design for web and mobile applications.</p>
                </div>
            </div>
            <div class="col-lg-6 order-1 order-lg-2">
                <div class="thumbnail text-left text-lg-right">
                    <div class=" text-right">
                        <img class="" src="{{asset('frontend/assets/images/shape/s1.svg')}}" width="40%"
                            alt="Slider images">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="moussaid-service-area ax-section-gap bg-color-white">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-xl-8">
                <div class="section-title text-left">
                    <span class="sub-title extra08-color wow" data-splitting>Approch</span>
                    <h2 class="title wow" data-splitting>Our Approach to work</h2>
                    <p class="subtitle-2 wow" data-splitting>We do know how to blend our dream team’s skills to
                        create
                        ideally-shaped smart and engaging web sites and mobile
                        applications.</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-6 col-12 mt--50 mt_md--40 mt_sm--30 move-up wow">
                <div class="moussaid-service-style--3">
                    <div class="icon">
                        <img src="{{asset('frontend/assets/images/icons/layer.svg')}}" alt="Icon Images">
                        <div class="text">1</div>
                    </div>
                    <div class="content">
                        <h4 class="title">1. Why?</h4>
                        <p>We believe in the power of design in software development. We thrive on building
                            relationships and helping good people and organizations succeed through a
                            collaborative process.

                        </p>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-6 col-12 mt--50 mt_md--40 mt_sm--30 move-up wow">
                <div class="moussaid-service-style--3 color-var--2">
                    <div class="icon">
                        <img src="{{asset('frontend/assets/images/icons/layer.svg')}}" alt="Icon Images">
                        <div class="text">2</div>
                    </div>
                    <div class="content">
                        <h4 class="title">2. How?</h4>
                        <p>When working on projects, we give value to precise timings, transparent project
                            management and on-time delivery. From traveling platforms to blockchain products, we
                            put our heart into your products and strive to exceed your expectations.</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-12 mt--50 mt_md--40 mt_sm--30 move-up wow">
                <div class="moussaid-service-style--3 color-var--3">
                    <div class="icon">
                        <img src="{{asset('frontend/assets/images/icons/layer.svg')}}" alt="Icon Images">
                        <div class="text">3</div>
                    </div>
                    <div class="content">
                        <h4 class="title">3. What?</h4>
                        <p>We develop web and mobile products with amazing interfaces and well-thought digital
                            design concepts. We look into products in a broader sense: bigger, more efficient,
                            focused on the user.

                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<div class="moussaid-service-area ax-section-gap bg-color-lightest">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="section-title text-left">
                    <span class="sub-title extra08-color wow" data-splitting>Values</span>
                    <h2 class="title wow" data-splitting>Our Values</h2>
                </div>
            </div>
        </div>
        <div class="row">

            <div class="col-lg-4 col-md-6 col-12 mt--10 mt_md--40 mt_sm--30">
                <div class="almoussaid-service text-center moussaid-control paralax-image">
                    <div class="inner">
                        <div class="icon">
                            <img src="{{asset('frontend/assets/images/icons/people.svg')}}" alt="Icon Images">

                        </div>
                        <div class="content">
                            <h4 class="title">People</h4>
                            <p>
                                We value relationships with our clients, partners and employees and put people’s
                                interests before anything else.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-12 mt--10 mt_md--40 mt_sm--30">
                <div class="almoussaid-service text-center moussaid-control paralax-image">
                    <div class="inner">
                        <div class="icon">
                            <img src="{{asset('frontend/assets/images/icons/meaning.svg')}}" alt="Icon Images">

                        </div>
                        <div class="content">
                            <h4 class="title">Meaning</h4>
                            <p>
                                Before creating a product, our team makes every effort to study the client’s
                                goals in depth.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-12 mt--10 mt_md--40 mt_sm--30">
                <div class="almoussaid-service text-center moussaid-control paralax-image">
                    <div class="inner">
                        <div class="icon">
                            <img src="{{asset('frontend/assets/images/icons/Aesthetics.svg')}}" alt="Icon Images">

                        </div>
                        <div class="content">
                            <h4 class="title">Aesthetics</h4>
                            <p>

                                We obsess over details and strive to be perfectionists in every piece of code
                                and every pixel of the interface.</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-6 col-12 mt--10 mt_md--40 mt_sm--30">
                <div class="almoussaid-service text-center moussaid-control paralax-image">
                    <div class="inner">
                        <div class="icon">
                            <img src="{{asset('frontend/assets/images/icons/development.svg')}}" alt="Icon Images">

                        </div>
                        <div class="content">
                            <h4 class="title">Development</h4>
                            <p>We never stop moving forward, developing, taking on new challenges. Each project
                                brings us fresh ideas and insights.</p>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-lg-4 col-md-6 col-12 mt--10 mt_md--40 mt_sm--30">
                <div class="almoussaid-service text-center moussaid-control paralax-image">
                    <div class="inner">
                        <div class="icon">
                            <img src="{{asset('frontend/assets/images/icons/communication.svg')}}" alt="Icon Images">

                        </div>
                        <div class="content">
                            <h4 class="title">Communication</h4>
                            <p>We always encourage our clients and team members to openly share their opinions
                                and views.</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-6 col-12 mt--10 mt_md--40 mt_sm--30">
                <div class="almoussaid-service text-center moussaid-control paralax-image">
                    <div class="inner">
                        <div class="icon">
                            <img src="{{asset('frontend/assets/images/icons/flexibility.svg')}}" alt="Icon Images">

                        </div>
                        <div class="content">
                            <h4 class="title">Flexibility</h4>
                            <p>We pursue a flexible approach to building apps, so that new insights and ideas
                                are taken into account.
                            </p>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
</div>

{{--
<div class="moussaid-team-area ax-section-gap ">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-xl-8">
                <div class="section-title text-left">
                    <span class="sub-title extra08-color wow" data-splitting>Team</span>
                    <h2 class="title wow" data-splitting>Staff</h2>

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-3 col-lg-6 col-md-6 col-sm-6 col-12 mt--60 mt_sm--30 mt_md--30">
                <div class="moussaid-team">
                    <div class="inner">
                        <div class="thumbnail paralax-image">

                            <img class="w-100" src="{{asset('frontend/assets/images/team/hamza.png')}}"
                                alt="Team Images">
                        </div>
                        <div class="content">
                            <h4 class="title">Hamza Rachid</h4>
                            <p class="subtitle">Graphic designer & a Marketer </p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xl-3 col-lg-6 col-md-6 col-sm-6 col-12 mt--60 mt_sm--30 mt_md--30">
                <div class="moussaid-team">
                    <div class="inner">
                        <div class="thumbnail paralax-image">
                            <img class="w-100" src="{{asset('frontend/assets/images/team/abdelilah.png')}}"
                                alt="Team Images">
                        </div>
                        <div class="content">
                            <h4 class="title">Abdelilah Ezzouini
                            </h4>
                            <p class="subtitle">CEO & Full stack developer </p>
                        </div>
                    </div>
                </div>
            </div>
            <!--
            <div class="col-xl-3 col-lg-6 col-md-6 col-sm-6 col-12 mt--60 mt_sm--30 mt_md--30">
                <div class="moussaid-team">
                    <div class="inner">
                        <div class="thumbnail paralax-image">
                            <img class="w-100" src="{{asset('frontend/assets/images/team/yaccine.png')}}"
                                alt="Team Images">
                        </div>
                        <div class="content">
                            <h4 class="title">Yacine Bou-ghalem
                            </h4>
                            <p class="subtitle">UXUI Designer & Frontend developer</p>
                        </div>
                    </div>
                </div>
            </div>
            -->


            <div class="col-xl-3 col-lg-6 col-md-6 col-sm-6 col-12 mt--60 mt_sm--30 mt_md--30">
                <div class="moussaid-team">
                    <div class="inner">
                        <div class="thumbnail paralax-image">
                            <img class="w-100" src="{{asset('frontend/assets/images/team/abdelwahab.jpeg')}}"
                                alt="Team Images">
                        </div>
                        <div class="content">
                            <h4 class="title">Abdelwahab Abourrabia
                            </h4>
                            <p class="subtitle">Sales Manager & Back-end developer</p>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-xl-3 col-lg-6 col-md-6 col-sm-6 col-12 mt--60 mt_sm--30 mt_md--30">
                <div class="moussaid-team">
                    <div class="inner">
                        <div class="thumbnail paralax-image">
                            <img class="w-100" src="{{asset('frontend/assets/images/team/jawad.png')}}"
                                alt="Team Images">
                        </div>
                        <div class="content">
                            <h4 class="title">Jawad Errehymni
                            </h4>
                            <p class="subtitle">Full stack developer</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
--}}

@endsection
