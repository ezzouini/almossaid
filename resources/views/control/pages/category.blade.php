@extends('control.layouts.app')

@section('title','Categories')

@section('content')

    @if( session('message') )
        <div class="alert alert-success my-2 p-3">
            {{ session('message') }}
        </div>
    @endif

    @if( $errors->any() )
        <div class="alert alert-danger my-2 p-3">
            <ul>
                @foreach( $errors->all() as $error )
                    <li> {{ $error }} </li>
                @endforeach
            </ul>
        </div>
    @endif
    

    @if( Request::route()->getName() == 'control.category' )
        <table class="table table-striped">
            <thead class="thead-dark">
                <tr>
                    <th width="150px"> <a class="btn btn-sm btn-primary" href="{{ route('control.category.create') }}"> <i class="fas fa-plus"></i> </a> </th>
                    <th width="50px" > </th>
                    <th> Name </th>
                </tr> 
            </thead>
            <tbody>
                @foreach( $categories as $category )
                    <tr>
                        <td>
                            <a href="{{ route('control.category.show', $category ) }}" class="mr-2 btn btn-sm btn-primary"> <i class="fas fa-external-link-alt"></i> <a>
                            <a href="{{ route('control.category.show', $category ) }}" class="mr-1 btn btn-sm btn-info"> <i class="fas fa-pencil-alt"></i> <a>
                            <label class="mr-1 mt-2 btn btn-sm btn-danger"> <i class="far fa-trash-alt"></i> <label>
                            <form action="{{ route('control.category.destroy',$category) }}" method="POST" class="d-none" >
                                @method('DELETE')
                                @csrf
                                <input type="submit" id="remove_item"  />
                            </form>
                        </td>
                        <td> @if( $category->image ) <img src="{{ asset('storage/'. $category->image->path ) }}" height="30"/> @endif </td>
                        <td> {{ $category->name }} </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

        {{ $categories->links() }}
    @elseif( Request::route()->getName() == 'control.category.create' )
        <form action="{{ route('control.category.store') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row mb-2">
                <div class="col-12 col-md-6">
                    <div class="form-group">
                        <label for="category_name">Category name</label>
                        <input name="name" type="text" class="form-control" id="category_name" value="{{ old('name') }}" onkeyup=" document.getElementById('category_slug').value = slugify( this.value ) " >
                    </div>
                </div>
                <div class="col-12 col-md-6">
                    <div class="form-group">
                        <label for="">Category name link</label>
                        <div class="input-group mb-2">
                            <div class="input-group-prepend">
                              <div class="input-group-text"> ...category/</div>
                            </div>
                            <input name="slug" type="text" class="form-control" id="category_slug" value="{{ old('slug') }}" placeholder="exemple_category">
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <input type="file" name="image" class="mb-2" id="category_image" accept="image/*" onchange="loadFile(event)">
                    <br />
                    <label for="category_image">
                        <img id="preview" class="img-thumbnail" src="https://demos.creative-tim.com/vue-black-dashboard-pro/img/image_placeholder.jpg" width="200px"/>
                    </label>
                </div>
            </div>
            
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    @elseif( Request::route()->getName() == 'control.category.show' )
        <form action="{{ route('control.category.update',$category) }}" method="POST" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="row mb-2">
                <div class="col-12 col-md-6">
                    <div class="form-group">
                        <label for="category_name">Category name</label>
                        <input name="name" type="text" class="form-control" id="category_name" value="{{ old('name') ?? $category->name }}" onkeyup=" document.getElementById('category_slug').value = slugify( this.value ) " >
                    </div>
                </div>
                <div class="col-12 col-md-6">
                    <div class="form-group">
                        <label for="">Category name link</label>
                        <div class="input-group mb-2">
                            <div class="input-group-prepend">
                            <div class="input-group-text"> ...category/</div>
                            </div>
                            <input name="slug" type="text" class="form-control" id="category_slug" value="{{ old('slug') ?? $category->slug }}" placeholder="exemple_category">
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <input type="file" name="image" class="mb-2" id="category_image" accept="image/*" onchange="loadFile(event)">
                    <br />
                    <label for="category_image">
                        <img id="preview" class="img-thumbnail" src="{{ $category->image ? asset('storage/'. $category->image->path ) : 'https://demos.creative-tim.com/vue-black-dashboard-pro/img/image_placeholder.jpg' }}" width="200px"/>
                    </label>
                </div>
            </div>
            
            <button type="submit" class="btn btn-primary">Submit</button>
            <label class="mt-2 btn btn-danger" for="remove_item"> <i class="far fa-trash-alt"></i> </label>
        </form>
        <form action="{{ route('control.category.destroy',$category) }}" method="POST" class="d-none" >
            @method('DELETE')
            @csrf
            <input type="submit" id="remove_item"  />
        </form>
    @endif
@endsection

@section('javascript')
<script>
    // Slugify a string
    function slugify(str)
    {
        str = str.replace(/^\s+|\s+$/g, '');

        // Make the string lowercase
        str = str.toLowerCase();

        // Remove accents, swap ñ for n, etc
        var from = "ÁÄÂÀÃÅČÇĆĎÉĚËÈÊẼĔȆÍÌÎÏŇÑÓÖÒÔÕØŘŔŠŤÚŮÜÙÛÝŸŽáäâàãåčçćďéěëèêẽĕȇíìîïňñóöòôõøðřŕšťúůüùûýÿžþÞĐđßÆa·/_,:;";
        var to   = "AAAAAACCCDEEEEEEEEIIIINNOOOOOORRSTUUUUUYYZaaaaaacccdeeeeeeeeiiiinnooooooorrstuuuuuyyzbBDdBAa------";
        for (var i=0, l=from.length ; i<l ; i++) {
            str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
        }

        // Remove invalid chars
        str = str.replace(/[^a-z0-9 -]/g, '') 
        // Collapse whitespace and replace by -
        .replace(/\s+/g, '-') 
        // Collapse dashes
        .replace(/-+/g, '-'); 

        return str;
    }

    var loadFile = function(event) {
        var output      = document.getElementById('preview');
        var eventTarget = event.target.files[0] ;
        if( eventTarget ){
            output.src = URL.createObjectURL( eventTarget );
            output.onload = function() {
            URL.revokeObjectURL(output.src) // free memory
            }
        }else{
            output.src = 'https://demos.creative-tim.com/vue-black-dashboard-pro/img/image_placeholder.jpg';
        }
    };
</script>
@endsection