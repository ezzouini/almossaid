@extends('control.layouts.app')

@section('title','Post')

@section('content')

    @if( session('message') )
        <div class="alert alert-success my-2 p-3">
            {{ session('message') }}
        </div>
    @endif

    @if( $errors->any() )
        <div class="alert alert-danger my-2 p-3">
            <ul>
                @foreach( $errors->all() as $error )
                    <li> {{ $error }} </li>
                @endforeach
            </ul>
        </div>
    @endif


    @if( Request::route()->getName() == 'control.post' )
        <table class="table table-striped">
            <thead class="thead-dark">
                <tr>
                    <th width="20%"> <a class="btn btn-sm btn-primary" href="{{ route('control.post.create') }}"> <i class="fas fa-plus"></i> </a> </th>
                    <th> title </th>
                    <th width="10%"> categories </th>
                    <th width="5%"> Views </th>
                    <th width="10%"> review </th>
                </tr>
            </thead>
            <tbody>
                @foreach( $posts as $post )
                    <tr class="{{ ( $post->status == 'INACTIVE' ? 'table-danger' : null) }}">
                        <td>
                            <a href="{{ route('post', $post->slug ) }}" target="_blank" class="mr-1 btn btn-sm btn-primary"> <i class="fas fa-external-link-alt"></i> <a>
                            <a href="{{ route('control.post.show', $post ) }}" class="btn btn-sm btn-info"> <i class="fas fa-pencil-alt"></i> <a>
                            <label class="mt-2 btn btn-sm btn-danger"> <i class="far fa-trash-alt"></i> <label>
                            <form action="{{ route('control.post.destroy',$post) }}" method="POST" class="d-none" >
                                @method('DELETE')
                                @csrf
                                <input type="submit" id="remove_item"  />
                            </form>
                        </td>
                        <td> {{ $post->title }} </td>
                        <td> {{ $post->categories->count() }} </td>
                        <td> {{ $post->view }} </td>
                        <td> .. % </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

        {{ $posts->links() }}
    @elseif( Request::route()->getName() == 'control.post.create' )
        <form action="{{ route('control.post.store') }}" method="POST" enctype="multipart/form-data" >
            @csrf
            <div class="row mb-2">
                <div class="col-12">
                    <div class="form-group">
                        <label for="post_title">Title</label>
                        <input name="title" type="text" class="form-control" id="post_title" value="{{ old('title') }}" onkeyup=" document.getElementById('post_slug').value = date_slugify(this.value) " >
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group">
                        <label for="">link slug</label>
                        <div class="input-group mb-2">
                            <div class="input-group-prepend">
                              <div class="input-group-text"> ...post/</div>
                            </div>
                            <input name="slug" type="text" class="form-control" id="post_slug" value="{{ old('slug') }}" placeholder="exemple_post">
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group">
                        <label for="post_categories">Categories</label>
                        <select multiple name="categories[]" class="form-control" id="post_categories" >
                            @foreach( $categories as $category )
                                <option value="{{ $category->id }}" > {{ $category->name }} </option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group">
                        <label for="post_tags">Keywords</label>
                        <input name="tags" type="text" class="form-control" id="post_tags" value="{{ old('tags') }}"  >
                    </div>
                </div>

                <div class="col-12">
                    <input type="file" name="image" class="mb-2" id="post_image" accept="image/*" onchange="loadFile(event)">
                    <br />
                    <label for="post_image">
                        <img id="preview" class="img-thumbnail" src="https://demos.creative-tim.com/vue-black-dashboard-pro/img/image_placeholder.jpg" width="300px"/>
                    </label>
                </div>
                <div class="col-12">
                    <div class="form-group">
                        <label for="post_body">Body</label>
                        <textarea rows="10" name="body" class="form-control" id="body_creator" >{{ old('body') }}</textarea>
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group">
                        <label for="post_status">Status</label>
                        <select name="status" class="form-control" id="post_status" >
                            <option value="ACTIVE" > ACTIVE </option>
                            <option value="INACTIVE" > INACTIVE </option>
                        </select>
                    </div>
                </div>
            </div>

            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    @elseif( Request::route()->getName() == 'control.post.show' )
        <form action="{{ route('control.post.update',$post) }}" method="POST" enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="row mb-2">
                <div class="col-12">
                    <div class="form-group">
                        <label for="post_title">Title</label>
                        <input name="title" type="text" class="form-control" id="post_title" value="{{ old('title') ?? $post->title }}"  >
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group">
                        <label for="">link slug</label>
                        <div class="input-group mb-2">
                            <div class="input-group-prepend">
                              <div class="input-group-text"> ...post/</div>
                            </div>
                            <input name="slug" type="text" class="form-control" id="post_slug" value="{{ old('slug') ?? $post->slug }}" placeholder="exemple_post">
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group">
                        <label for="post_categories">Categories</label>
                        <select multiple name="categories[]" class="form-control" id="post_categories" >
                            @foreach( $categories as $category )
                                <option value="{{ $category->id }}" {{ array_search($category->id, array_column($post->categories->toArray() , 'id' )) !== false ? 'selected' : null }} > {{ $category->name }} </option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="col-12">
                    <div class="form-group">
                        @php
                            $tags = [];
                            foreach ( $post->tags as $tag ) {
                                array_push( $tags , $tag->name );
                            }
                            $tagsStr = implode(",", $tags );
                        @endphp

                        <label for="post_tags">Keywords</label>
                        <input name="tags" type="text" class="form-control" id="post_tags" value="{{ old('tags') ?? $tagsStr }}"  >
                    </div>
                </div>

                <div class="col-12">
                    <input type="file" name="image" class="mb-2" id="post_image" accept="image/*" onchange="loadFile(event)">
                    <br />
                    <label for="post_image">
                        <img id="preview" class="img-thumbnail" src="{{ $post->image ? asset('storage/'. $post->image->path ) : 'https://demos.creative-tim.com/vue-black-dashboard-pro/img/image_placeholder.jpg' }}" width="200px"/>
                    </label>
                </div>

                <div class="col-12">
                    <div class="form-group">
                        <label for="post_body">Body</label>
                        <textarea rows="10" name="body" class="form-control" id="body_creator" >{{ old('body') ?? $post->body }}</textarea>
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group">
                        <label for="post_status">Status</label>
                        <select name="status" class="form-control" id="post_status" >
                            <option value="ACTIVE"   {{ $post->status == 'ACTIVE' ? 'selected' : null }} > ACTIVE </option>
                            <option value="INACTIVE" {{ $post->status == 'INACTIVE' ? 'selected' : null }} > INACTIVE </option>
                        </select>
                    </div>
                </div>
            </div>

            <button type="submit" class="btn btn-primary">Submit</button>
            <label class="mt-2 btn btn-danger" for="remove_item"> <i class="far fa-trash-alt"></i> </label>
        </form>
        <form action="{{ route('control.post.destroy',$post) }}" method="POST" class="d-none" >
            @method('DELETE')
            @csrf
            <input type="submit" id="remove_item"  />
        </form>
    @endif


@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('multiple-select-js-master/dist/css/multiple-select.css') }}" />
@endsection

@section('javascript')

    <script src="{{ asset('multiple-select-js-master/dist/js/multiple-select.js') }}"></script>

    <script src="https://cdn.tiny.cloud/1/44xefxssvu8rliqtvzhg9xynqlgzcxj4bkmckx0lb30t7u1i/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>

    <script>
    // multi select categories
    new MultipleSelect('#post_categories', {
        placeholder: 'Select Categories'
    })
    // WISIWIG editor

    function example_image_upload_handler (blobInfo, success, failure, progress) {
        var xhr, formData;

        xhr = new XMLHttpRequest();
        xhr.withCredentials = false;
        xhr.open('POST', '{{ url("/") }}/api/image');
        xhr.setRequestHeader("Accept", "application/json");

        formData = new FormData();
        formData.append('image', blobInfo.blob(), blobInfo.filename());

        xhr.send(formData);

        xhr.upload.onprogress = function (e) {
            progress(e.loaded / e.total * 100);
        };

        xhr.onload = function() {
            var json ;

            if (xhr.status === 403) {
                failure('HTTP Error: ' + xhr.status, { remove: true });
                return;
            }

            if (xhr.status < 200 || xhr.status >= 300) {
                var text = '<br />';
                xhr.responseText.errors.image.forEach(err=> {
                    text+= err + '<br />';
                });
                console.log( text );
                failure('HTTP Error: ' + xhr.status + ' ' + text );
                return;
            }

            var json = JSON.parse(xhr.responseText);
            /*
            if (!json || typeof json.location != 'string') {
                failure('Invalid JSON: ' + xhr.responseText);
                return;
            }*/

            success(json.image);
        };

        xhr.onerror = function () {
            failure('Image upload failed due to a XHR Transport error. Code: ' + xhr.status);
        };

    };

    tinymce.init({
        selector: '#body_creator',
        height: 400,
        plugins: [
            'a11ychecker advcode casechange formatpainter linkchecker autolink lists checklist permanentpen powerpaste table advtable tinycomments tinymcespellchecker charmap',
            'mediaembed pageembed imagetools image media preview print fullscreen codesample pagebreak'
        ],

        toolbar: [
            'undo redo | bold italic underline | fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | numlist bullist | forecolor backcolor removeformat |',
            'pagebreak | charmap | gallery image media link anchor codesample | ltr rtl | permanentpen table | preview print fullscreen',
        ],
        toolbar_mode: 'floating',

        tinycomments_mode: 'embedded',
        tinycomments_author: '{{ auth()->user()->name }}',

        //fullscreen_native: true ,

        automatic_uploads: true,

        images_upload_handler: example_image_upload_handler,
        image_advtab: true,
        image_caption: true,
        image_dimensions: false,
        //image_prepend_url: "https://www.tinymce.com/images/",
        relative_urls : false,
        remove_script_host : false,
        convert_urls : true,

        image_class_list: [
            {title: 'None', value: ''},
            {title: 'Responsive', value: 'img-fluid'},
            {title: 'Thumbnail', value: 'img-thumbnail'},
        ],

        setup: function (editor) {
            editor.ui.registry.addButton('gallery', {
                text: 'Gallery',
                icon : 'gallery',
                onAction: async function (_) {
                    $('#image').val('');

                    var htmlGalery = "";
                    await fetch('{{ url("/") }}/api/images')
                        .then(response => response.json())
                        .then(data => {
                            htmlGalery+= "<div class='row'>";
                                data.forEach(img => {
                                    htmlGalery+= "<div class='item col-2 p-2' onclick='selectItem(this)' > <img src='"+img.value+"' class='rounded shadow-sm' style='max-width: 100%; height: auto;' /> </div>";
                                });
                            htmlGalery+= "</div>";
                        });
                    var html = "<div class='gallery-div'><input type='hidden' id='image'/> "+htmlGalery+" </div>";

                    tinyMCE.activeEditor.windowManager.open({
                        title: 'My Gallery', // The dialog's title - displayed in the dialog header
                        size: 'large',
                        body: {
                            type: 'panel', // The root body type - a Panel or TabPanel
                            items: [ // A list of panel components
                                {
                                    type: 'htmlpanel', // A HTML panel component
                                    html: html
                                }
                            ]
                        },
                        buttons: [
                            {
                                type: 'cancel',
                                name: 'closeButton',
                                text: 'Cancel'
                            },
                            {
                                type: 'submit',
                                name: 'submitButton',
                                text: 'Insert To Editor',
                                primary: true
                            }
                        ],
                        onSubmit: function (api) {
                            var data = api.getData();
                            var source = $('#image').val();
                            //var source = document.getElementById('#image').value;
                            console.log( source );

                            if(source != ''){
                                editor.focus();
                                editor.selection.setContent('<img src="'+source+'" class="img-fluid" />');
                                api.close();
                                //source = null ;
                            }else{
                                alert("Please select an image.");
                            }
                        }
                    });
                }
            });
        },

    });
    // Slugify a string
    function slugify(str)
    {
        str = str.replace(/^\s+|\s+$/g, '');

        // Make the string lowercase
        str = str.toLowerCase();

        // Remove accents, swap ñ for n, etc
        var from = "ÁÄÂÀÃÅČÇĆĎÉĚËÈÊẼĔȆÍÌÎÏŇÑÓÖÒÔÕØŘŔŠŤÚŮÜÙÛÝŸŽáäâàãåčçćďéěëèêẽĕȇíìîïňñóöòôõøðřŕšťúůüùûýÿžþÞĐđßÆa·/_,:;";
        var to   = "AAAAAACCCDEEEEEEEEIIIINNOOOOOORRSTUUUUUYYZaaaaaacccdeeeeeeeeiiiinnooooooorrstuuuuuyyzbBDdBAa------";
        for (var i=0, l=from.length ; i<l ; i++) {
            str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
        }

        // Remove invalid chars
        str = str.replace(/[^a-z0-9 -]/g, '')
        // Collapse whitespace and replace by -
        .replace(/\s+/g, '-')
        // Collapse dashes
        .replace(/-+/g, '-');

        return str;
    }
    function date_slugify(value){
        return new Date().toLocaleDateString('en-US').replaceAll('/', '-') + '-' + slugify( value );
    }

    var loadFile = function(event) {
        var output      = document.getElementById('preview');
        var eventTarget = event.target.files[0] ;
        if( eventTarget ){
            output.src = URL.createObjectURL( eventTarget );
            output.onload = function() {
            URL.revokeObjectURL(output.src) // free memory
            }
        }else{
            output.src = 'https://demos.creative-tim.com/vue-black-dashboard-pro/img/image_placeholder.jpg';
        }
    }

    function selectItem (item){
        $('.gallery-div .item').each(function(index){
            $(this).removeClass('border');
        });
        $(item).addClass('border');
        $('#image').val($(item).find('img').attr('src'));
    }

    </script>
@endsection
