<div class="moussaid-blog-area ax-section-gap bg-color-white">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="section-title text-left">
                    <span class="sub-title primary-color">Blog</span>
                    <h2 class="title">Our latest articles and publications</h2>

                </div>
            </div>
        </div>
        <div class="row blog-list-wrapper mt--20">

            @foreach ($latestposts as $post)
            <div class="col-lg-6 col-md-6 col-12">
                <div class="moussaid-blog moussaid-control mt--40 active move-up wow">
                    <div class="content">
                        <div class="content-wrap paralax-image">
                            <div class="inner">
                                @foreach( $post->categories as $category )
                                <a href="{{ route('category', ( $category->slug ?? $category ) ) }}"><span
                                        class="category">{{ $category->name }}</span></a>
                                @endforeach

                                <h5 class="title"><a href="{{ route('post', $post->slug ) }}">{{ $post->title }}</a>
                                </h5>
                                <p> {!! substr(strip_tags($post->description), 0, 100) !!}...</p>
                                <small>{{ $post->timeAgo }}</small>
                            </div>
                        </div>
                    </div>
                    <div class="thumbnail">
                        <div class="image">
                            <img src="{{ asset('storage/'. $post->image->path ) }}" alt="Blog images">
                        </div>
                    </div>
                </div>
            </div>
            @endforeach




        </div>
    </div>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6 text-center">
                <a class="moussaid-button mt--30  btn-large btn-transparent" href="#"><span class="button-text">All
                        posts</span><span class="button-icon"></span></a>
            </div>
        </div>
    </div>
</div>