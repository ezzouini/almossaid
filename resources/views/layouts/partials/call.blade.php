<div class="moussaid-call-to-action-area shape-position ax-section-gap bg-color-darkest">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="moussaid-call-to-action">
                    <div class="section-title text-center">
                        <span class="sub-title primary-color wow" data-splitting>Let's work together</span>
                        <h2 class="title wow whitecolor" data-splitting>Need a successful project?</h2>
                        <a class="moussaid-button btn-large btn-transparent" href="#"><span
                                class="button-text ">Estimate Project</span><span class="button-icon"></span></a>
                        <div class="callto-action">
                            <span class="text wow whitecolor" data-splitting>Or call us now</span>
                            <span class="wow whitecolor" data-splitting><i class="fas fa-phone-alt"></i> <a
                                    class="whitecolor" href="#">+212 670-039289</a></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
