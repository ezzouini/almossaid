<div class="moussaid-service-area ax-section-gap ">
    <div class="container">
        <div class="row">
            <div class="col-lg-7">
                <div class="section-title text-left pb--45">
                    <span class="sub-title extra08-color wow" data-splitting>SERVICES</span>
                    <h2 class="title wow" data-splitting><span>We’re The Whole Package</span></h2>
                </div>
            </div>
        </div>
        <div class="row">

            <div class="col-lg-3 col-md-6 col-sm-6 col-12 move-up wow ">
                <div class="almoussaid-service text-center moussaid-control paralax-image">
                    <div class="inner">
                        <div class="icon">
                            <img src="{{asset('frontend/assets/images/icons/devweb.svg')}}" alt="Icon Images">

                        </div>
                        <div class="content">
                            <h4 class="title">Web Development</h4>
                            <p> We build people first solutions
                                Frontend, Backend, API, QA</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-6 col-sm-6 col-12 ">
                <div class="almoussaid-service text-center moussaid-control paralax-image">
                    <div class="inner">
                        <div class="icon">
                            <img src="{{asset('frontend/assets/images/icons/mobile.svg')}}" alt="Icon Images">

                        </div>
                        <div class="content">
                            <h4 class="title">Mobile Development</h4>
                            <p> Making applications that work
                                Android, iOS, QA</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-6 col-sm-6 col-12 move-up wow">
                <div class="almoussaid-service text-center moussaid-control paralax-image">
                    <div class="inner">
                        <div class="icon">
                            <img src="{{asset('frontend/assets/images/icons/uxui.svg')}}" alt="Icon Images">

                        </div>
                        <div class="content">
                            <h4 class="title">UX/UI Design</h4>
                            <p> Research, UX Strategy,
                                UI Design, Usability Testing</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-12 move-up wow">
                <div class="almoussaid-service text-center moussaid-control paralax-image">
                    <div class="inner">
                        <div class="icon">
                            <img src="{{asset('frontend/assets/images/icons/team.svg')}}" alt="Icon Images">

                        </div>
                        <div class="content">
                            <h4 class="title">Dedicated team</h4>
                            <p> Analysis, Team Selection,
                                Operation, Launch</p>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
</div>