<div class="moussaid-slider-area moussaid-slide-activation">
    <div class="moussaid-slide banner-technology bg_image bg_image--1 ">
        <div class="container">
            <div class="row align-items-center pt_md--60 mt_sm--60">
                <div class="col-lg-7 col-12 order-2 order-lg-1">
                    <div class="content pt_md--30 pt_sm--30">
                        <h1 class="moussaid-display-1 prim-color wow slideFadeInUp" data-wow-duration="1s"
                            data-wow-delay="500ms">Where others see problems<br />
                            We see solutions.</h1>
                        <h2 class="moussaid-display-3 second-color wow slideFadeInUp" data-wow-duration="1s"
                            data-wow-delay="500ms">Building reliable software solutions for your business.
                        </h2>
                        <p class="subtitle-3 wow slideFadeInUp" data-wow-duration="1s" data-wow-delay="800ms">
                            Almossaid is a professional software development outsourcing company. We strive to
                            succeed, we crave innovation, we make a difference
                        </p>
                        <a class="moussaid-button btn-large btn-transparent wow slideFadeInUp" data-wow-duration="1s"
                            data-wow-delay="1300ms" href="/contact"><span class="button-text">ESTIMATE PROJECT</span><span
                                class="button-icon"></span></a>
                    </div>

                </div>


            </div>
        </div>
        <div class="topskew-thumbnail-group almousaid-crea text-left text-lg-right">
            <img src="{{asset('frontend/assets/images/bg/crea01.svg')}}" alt="Almoudaid">
        </div>
    </div>
</div>
