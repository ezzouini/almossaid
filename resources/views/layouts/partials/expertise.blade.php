<div class="moussaid-service-area ax-section-gap backgound-dots">
    <div class="container">

        <div class="row">

            <div class="col-lg-5 col-12">
                <div class="section-title text-left">
                    <span class="sub-title extra08-color wow" data-splitting>Our Expertise</span>
                    <h2 class="title wow" data-splitting>Who We Are?</h2>
                    <p class="subtitle-2 wow pr--0" data-splitting>We believe in the power of design in software
                        development. We thrive on building relationships and helping good people and
                        organizations succeed through a collaborative process.
                    </p>
                    <div class="view-all-portfolio-button mt--40">
                        <a class="moussaid-button btn-large btn-transparent" href="#"><span class="button-text">Get
                                to Know Us Better</span><span class="button-icon"></span></a>
                    </div>
                </div>
            </div>

            <div class="col-lg-6 offset-xl-1 col-12 mt_md--40 mt_sm--40">
                <div class="row">

                    <div class="col-lg-6 col-md-6 col-sm-6 col-6">
                        <div class="almoussaid-expertise text-left counter-1 move-up wow">
                            <div class="inner">
                                <div class="icon">
                                    <img src="{{asset('frontend/assets/images/icons/e-commerce.svg')}}"
                                        alt="Icon Images">
                                </div>

                                <div class="icon-go">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="14.012" height="14.012"
                                        viewBox="0 0 14.012 14.012">
                                        <g id="Icon_feather-arrow-up-right" data-name="Icon feather-arrow-up-right"
                                            transform="translate(-9.086 -9.086)">
                                            <path id="Path_34125" data-name="Path 34125" d="M10.5,21.684,21.684,10.5"
                                                fill="none" stroke="#9D9D9D" stroke-linecap="round"
                                                stroke-linejoin="round" stroke-width="2" />
                                            <path id="Path_34126" data-name="Path 34126" d="M10.5,10.5H21.684V21.684"
                                                fill="none" stroke="#9D9D9D" stroke-linecap="round"
                                                stroke-linejoin="round" stroke-width="2" />
                                        </g>
                                    </svg>
                                </div>

                                <div class="content">
                                    <h4 class="title">E-Commerce</h4>
                                    <p>Online stores, retail marketplaces and ecommerce startups.</p>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="col-lg-6 col-md-6 col-sm-6 col-6">
                        <div class="almoussaid-expertise text-left color-style-two mt--60 mt_mobile--40 move-up wow">
                            <div class="inner">
                                <div class="icon">
                                    <img src="{{asset('frontend/assets/images/icons/e-learning.svg')}}"
                                        alt="Icon Images">
                                </div>

                                <div class="icon-go">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="14.012" height="14.012"
                                        viewBox="0 0 14.012 14.012">
                                        <g id="Icon_feather-arrow-up-right" data-name="Icon feather-arrow-up-right"
                                            transform="translate(-9.086 -9.086)">
                                            <path id="Path_34125" data-name="Path 34125" d="M10.5,21.684,21.684,10.5"
                                                fill="none" stroke="#9D9D9D" stroke-linecap="round"
                                                stroke-linejoin="round" stroke-width="2" />
                                            <path id="Path_34126" data-name="Path 34126" d="M10.5,10.5H21.684V21.684"
                                                fill="none" stroke="#9D9D9D" stroke-linecap="round"
                                                stroke-linejoin="round" stroke-width="2" />
                                        </g>
                                    </svg>
                                </div>

                                <div class="content">
                                    <h4 class="title">E-learning</h4>
                                    <p>Online training, learning management systems, and mobile apps.</p>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="col-lg-6 col-md-6 col-sm-6 col-6">
                        <div class="almoussaid-expertise text-left color-style-three mt_mobile--40 move-up wow">
                            <div class="inner">
                                <div class="icon">
                                    <img src="{{asset('frontend/assets/images/icons/travel.svg')}}" alt="Icon Images">
                                </div>

                                <div class="icon-go">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="14.012" height="14.012"
                                        viewBox="0 0 14.012 14.012">
                                        <g id="Icon_feather-arrow-up-right" data-name="Icon feather-arrow-up-right"
                                            transform="translate(-9.086 -9.086)">
                                            <path id="Path_34125" data-name="Path 34125" d="M10.5,21.684,21.684,10.5"
                                                fill="none" stroke="#9D9D9D" stroke-linecap="round"
                                                stroke-linejoin="round" stroke-width="2" />
                                            <path id="Path_34126" data-name="Path 34126" d="M10.5,10.5H21.684V21.684"
                                                fill="none" stroke="#9D9D9D" stroke-linecap="round"
                                                stroke-linejoin="round" stroke-width="2" />
                                        </g>
                                    </svg>
                                </div>

                                <div class="content">
                                    <h4 class="title">Travel & Hospitality</h4>
                                    <p>Solutions for tour operators, travel & hospitality agencies and corporate
                                        clients.</p>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="col-lg-6 col-md-6 col-sm-6 col-6">
                        <div class="almoussaid-expertise text-left color-style-four mt--60 mt_mobile--40 move-up wow">
                            <div class="inner">
                                <div class="icon">
                                    <img src="{{asset('frontend/assets/images/icons/indusrty.svg')}}" alt="Icon Images">
                                </div>

                                <div class="icon-go">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="14.012" height="14.012"
                                        viewBox="0 0 14.012 14.012">
                                        <g id="Icon_feather-arrow-up-right" data-name="Icon feather-arrow-up-right"
                                            transform="translate(-9.086 -9.086)">
                                            <path id="Path_34125" data-name="Path 34125" d="M10.5,21.684,21.684,10.5"
                                                fill="none" stroke="#9D9D9D" stroke-linecap="round"
                                                stroke-linejoin="round" stroke-width="2" />
                                            <path id="Path_34126" data-name="Path 34126" d="M10.5,10.5H21.684V21.684"
                                                fill="none" stroke="#9D9D9D" stroke-linecap="round"
                                                stroke-linejoin="round" stroke-width="2" />
                                        </g>
                                    </svg>
                                </div>

                                <div class="content">
                                    <h4 class="title">Industry</h4>
                                    <p>Outsourcing software development services to beat off market competition
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>