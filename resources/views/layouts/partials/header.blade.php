<header class="ax-header haeder-default light-logo-version header-transparent moussaid-header-sticky">
    <div class="header-wrapper">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-3 col-md-6 col-sm-6 col-8">
                    <div class="logo">
                        <a href="{{ route('home') }}">
                            <img src="{{asset('frontend/assets/images/shape/s1.svg')}}" alt='Almossaid' width="76px" />
                        </a>
                    </div>
                </div>
                <div class="col-lg-9 col-md-6 col-sm-6 col-4">
                    <div class="mainmenu-wrapepr">
                        <nav class="mainmenu-nav d-none d-lg-block">
                            <ul class="mainmenu">
                                <li class="has-dropdown"><a href="#">Services</a>
                                    <ul class="moussaid-submenu">
                                        <li><a href="{{ route('services.web_development') }}">Web Development</a></li>
                                        <li><a href="{{ route('services.mobile_development') }}">Mobile Development</a></li>
                                        <li><a href="{{ route('services.uxui_design') }}">UX/UI Design</a></li>
                                        <li><a href="{{ route('services.dedicated_team') }}">Dedicated team</a></li>
                                    </ul>
                                </li>
                                <li><a href="{{ route('cases') }}">Portfolio</a>

                                </li>

                                <li><a href="{{ route('expertise') }}">Expertise</a></li>
                                <li><a href="{{ route('company') }}">Company</a></li>

                                {{-- <li><a href="/blog">Blog</a></li> --}}

                            </ul>
                        </nav>
                        <div class="ax-header-button ml--40 ml_lg--10 d-none d-sm-block">
                            <a class="moussaid-button btn-solid btn-extra02-color" href="{{ route('contact') }}"><span
                                    class="button-text">Contact us</span><span class="button-icon"></span></a>
                        </div>
                        <div class="ax-menubar popup-navigation-activation d-block d-lg-none ml_sm--20 ml_md--20">
                            <div>
                                <i></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>

<div class="popup-mobile-manu">
    <div class="inner">
        <div class="mobileheader">
            <div class="logo">
                <a href="{{ route('home') }}">
                    <img src="{{asset('frontend/assets/images/shape/s1.svg')}}" alt="Almossaid" width="70px">
                </a>
            </div>
            <a class="close-menu" href="#"></a>
        </div>
        <div class="menu-item">
            <ul class="mainmenu-item">
                <li><a href="{{ route('home') }}">Home</a></li>
                <li class="has-children"><a href="#">Services</a>
                    <ul class="submenu">
                        <li><a href="{{ route('services.web_development') }}">Web Development</a></li>
                        <li><a href="{{ route('services.mobile_development') }}">Mobile Development</a></li>
                        <li><a href="{{ route('services.uxui_design') }}">UX/UI Design</a></li>
                        <li><a href="{{ route('services.dedicated_team') }}">Dedicated team</a></li>
                    </ul>
                </li>
                <li><a href="{{ route('expertise') }}">Expertise</a></li>
                <li><a href="{{ route('company') }}">Company</a></li>
                {{-- <li><a href="/blog">Blog</a></li> --}}

                <li><a href="{{ route('contact') }}">Contact</a></li>
            </ul>
        </div>

    </div>
</div>
