<div class="moussaid-service-area ax-section-gap ">
    <div class="container">
        <div class="row">
            <div class="col-lg-7">
                <div class="section-title text-left pb--45">
                    <span class="sub-title extra08-color wow" data-splitting>Case studies</span>
                    <h3 class="title wow" data-splitting><span>Our Cases</span></h3>
                </div>
            </div>
        </div>
        <div class="row">

            <div class="col-lg-6 col-12 move-up wow ">
                <div class="almoussaid-cases text-center moussaid-control ">
                    <div class="almoussaid-cases-left text-left">
                        <div class="section-title text-left">
                            <span class="sub-title primary-color wow animated" style="visibility: visible;">Web Application E-Commerce 🛍️</span>
                            <h4 class="title wow animated" style="visibility: visible;">ELECTROMENAGER</h4>
                            <p class="subtitle-2 wow animated" style="visibility: visible;">
                                HTML/CSS3, VueJs, Laravel, MySQL
                            </p>
                            <a class="moussaid-button btn-large btn-transparent" href="https://alloelectromenager.com/">
                                <span class="button-text">Visiting site</span>
                                <span class="button-icon"></span>
                            </a>
                        </div>
                    </div>

                    <div class="almoussaid-cases-right text-left">
                        <img src="{{asset('frontend/assets/images/portfolio/case1.png')}}" alt="" />
                    </div>
                </div>
            </div>

            <div class="col-lg-6 col-12 move-up wow ">
                <div class="almoussaid-cases text-center moussaid-control ">
                    <div class="almoussaid-cases-left text-left">
                        <div class="section-title text-left">
                            <span class="sub-title primary-color wow animated" style="visibility: visible;"> Mobile App 🛍️</span>
                            <h4 class="title wow animated" style="visibility: visible;">Tisa app </h4>
                            <p class="subtitle-2 wow animated" style="visibility: visible;">
                                Node js, React native, mysql </p>
                            <a class="moussaid-button btn-large btn-transparent" href="{{ route('cases.tisaapp') }}">
                                <span class="button-text">Project</span>
                                <span class="button-icon"></span>
                            </a>
                        </div>
                    </div>

                    <div class="almoussaid-cases-right text-left">
                        <img src="{{asset('frontend/assets/images/portfolio/case2.png')}}" alt="" />
                    </div>
                </div>
            </div>

            <div class="col-lg-6 col-12 move-up wow ">
                <div class="almoussaid-cases text-center moussaid-control ">
                    <div class="almoussaid-cases-left text-left">
                        <div class="section-title text-left">
                            <span class="sub-title primary-color wow animated" style="visibility: visible;">Web Application E-Commerce 🛍️</span>
                            <h4 class="title wow animated" style="visibility: visible;">Energy market </h4>
                            <p class="subtitle-2 wow animated" style="visibility: visible;">
                                Laravel, React native
                            </p>
                            <a class="moussaid-button btn-large btn-transparent" href="http://energymarket.ma/">
                                <span class="button-text">Visiting site</span>
                                <span class="button-icon"></span>
                            </a>
                        </div>
                    </div>

                    <div class="almoussaid-cases-right text-left">
                        <img src="{{asset('frontend/assets/images/portfolio/energy.png')}}" alt="" />
                    </div>
                </div>
            </div>

            <div class="col-lg-6 col-12 move-up wow ">
                <div class="almoussaid-cases text-center moussaid-control ">
                    <div class="almoussaid-cases-left text-left">
                        <div class="section-title text-left">
                            <span class="sub-title primary-color wow animated" style="visibility: visible;">Web Application Real Estate Platform 🏢</span>
                            <h4 class="title wow animated" style="visibility: visible;">Immo101</h4>
                            <p class="subtitle-2 wow animated" style="visibility: visible;">
                                HTML/CSS3, JavaScript, Ajax, Laravel, MySql
                            </p>
                            <a class="moussaid-button btn-large btn-transparent" href="https://immo101.ma/">
                                <span class="button-text">Visiting site</span>
                                <span class="button-icon"></span>
                            </a>
                        </div>
                    </div>

                    <div class="almoussaid-cases-right text-left">
                        <img src="{{asset('frontend/assets/images/portfolio/immo.png')}}" alt="" />
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-12 move-up wow ">
                <div class="almoussaid-cases text-center moussaid-control ">
                    <div class="almoussaid-cases-left text-left">
                        <div class="section-title text-left">
                            <span class="sub-title primary-color wow animated" style="visibility: visible;">Web Application Hotel 🏨</span>
                            <h4 class="title wow animated" style="visibility: visible;">Riad Milouda </h4>
                            <p class="subtitle-2 wow animated" style="visibility: visible;">
                                HTML/CSS3, JavaScript, Ajax, Laravel, MySql
                            </p>
                            <a class="moussaid-button btn-large btn-transparent" href="https://riad-milouda.com/">
                                <span class="button-text">Visiting site</span>
                                <span class="button-icon"></span>
                            </a>
                        </div>
                    </div>

                    <div class="almoussaid-cases-right text-left">
                        <img src="{{asset('frontend/assets/images/portfolio/riad.png')}}" alt="" />
                    </div>
                </div>
            </div>

            <div class="col-lg-6 col-12 move-up wow ">
                <div class="almoussaid-cases text-center moussaid-control ">
                    <div class="almoussaid-cases-left text-left">
                        <div class="section-title text-left">
                            <span class="sub-title primary-color wow animated" style="visibility: visible;">Web Application Catalogue 🛍️ </span>
                            <h4 class="title wow animated" style="visibility: visible;">El Ouasti</h4>
                            <p class="subtitle-2 wow animated" style="visibility: visible;">
                                HTML/CSS3, JavaScript, Ajax, Laravel, MySql
                            </p>
                            <a class="moussaid-button btn-large btn-transparent" href="http://elouasti.com/public/">
                                <span class="button-text">Visiting site</span>
                                <span class="button-icon"></span>
                            </a>
                        </div>
                    </div>

                    <div class="almoussaid-cases-right text-left">
                        <img src="{{asset('frontend/assets/images/portfolio/ousti.png')}}" alt="" />
                    </div>
                </div>
            </div>

            <div class="col-lg-6 col-12 move-up wow ">
                <div class="almoussaid-cases text-center moussaid-control ">
                    <div class="almoussaid-cases-left text-left">
                        <div class="section-title text-left">
                            <span class="sub-title primary-color wow animated" style="visibility: visible;">Web Application Language school 🏫 </span>
                            <h4 class="title wow animated" style="visibility: visible;"> RMT-Ruhr </h4>
                            <p class="subtitle-2 wow animated" style="visibility: visible;">
                                HTML/CSS3, JavaScript, Ajax, Laravel, MySql
                            </p>
                            <a class="moussaid-button btn-large btn-transparent" href="https://rmt-ruhr.de/">
                                <span class="button-text">Visiting site</span>
                                <span class="button-icon"></span>
                            </a>
                        </div>
                    </div>

                    <div class="almoussaid-cases-right text-left">
                        <img src="{{asset('frontend/assets/images/portfolio/rmt.png')}}" alt="" />
                    </div>
                </div>
            </div>

            <div class="col-lg-6 col-12 move-up wow ">
                <div class="almoussaid-cases text-center moussaid-control ">
                    <div class="almoussaid-cases-left text-left">
                        <div class="section-title text-left">
                            <span class="sub-title primary-color wow animated" style="visibility: visible;">Web Application Creche 🏫 </span>
                            <h4 class="title wow animated" style="visibility: visible;">
                                Institution Bakrim - Platforme Ecole
                            </h4>
                            <p class="subtitle-2 wow animated" style="visibility: visible;">
                                HTML/CSS3, JavaScript, Ajax, PHP, MySql
                            </p>
                            <a class="moussaid-button btn-large btn-transparent" href="http://institution-bakrim.com/">
                                <span class="button-text">Visiting site</span>
                                <span class="button-icon"></span>
                            </a>
                        </div>
                    </div>

                    <div class="almoussaid-cases-right text-left">
                        <img src="{{asset('frontend/assets/images/portfolio/bakrim.png')}}" alt="" />
                    </div>
                </div>
            </div>


            <div class="col-lg-6 col-12 move-up wow ">
                <div class="almoussaid-cases text-center moussaid-control ">
                    <div class="almoussaid-cases-left text-left">
                        <div class="section-title text-left">
                            <span class="sub-title primary-color wow animated" style="visibility: visible;">Web Design 🖌 </span>
                            <h4 class="title wow animated" style="visibility: visible;">
                                Hypnoses Bienveillantes
                            </h4>
                            <p class="subtitle-2 wow animated" style="visibility: visible;">
                                HTML/CSS3, JavaScript, WordPress
                            </p>
                            <a class="moussaid-button btn-large btn-transparent" href="https://hypnoses-bienveillantes.fr/">
                                <span class="button-text">Visiting site</span>
                                <span class="button-icon"></span>
                            </a>
                        </div>
                    </div>

                    <div class="almoussaid-cases-right text-left">
                        <img src="{{asset('frontend/assets/images/portfolio/hypnose.png')}}" alt="" />
                    </div>
                </div>
            </div>

            <div class="col-lg-6 col-12 move-up wow ">
                <div class="almoussaid-cases text-center moussaid-control ">
                    <div class="almoussaid-cases-left text-left">
                        <div class="section-title text-left">
                            <span class="sub-title primary-color wow animated" style="visibility: visible;">Web Portfolio 🛍️ </span>
                            <h4 class="title wow animated" style="visibility: visible;">
                                Mica on earth
                            </h4>
                            <p class="subtitle-2 wow animated" style="visibility: visible;">
                                HTML/CSS3, Bootstrap , Jquery
                            </p>
                            <a class="moussaid-button btn-large btn-transparent" href="https://micaonearth.com/">
                                <span class="button-text">Visiting site</span>
                                <span class="button-icon"></span>
                            </a>
                        </div>
                    </div>

                    <div class="almoussaid-cases-right text-left">
                        <img src="{{asset('frontend/assets/images/portfolio/micaonearth.png')}}" alt="" />
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6 text-center">
                <a class="moussaid-button  mt--30 btn-large btn-transparent" href="/cases">
                    <span class="button-text">All cases</span>
                    <span class="button-icon"></span>
                </a>
            </div>
        </div>
    </div>
</div>
