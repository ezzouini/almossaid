<footer class="moussaid-footer footer-default theme-gradient-2">
    <div class="bg_image--2">
        <div class="ft-social-icon-wrapper ax-section-gapTop">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <ul class="ft-social-share d-flex justify-content-center liststyle flex-wrap">
                            <li><a target="_blank" href="https://www.facebook.com/almossaidweb"><i class="fab fa-facebook-f"></i></a></li>
                            <li><a target="_blank" href="https://www.instagram.com/almossaidweb/"><i class="fab fa-instagram"></i></a></li>
                            <li><a target="_blank" href="https://www.linkedin.com/company/almossaid"><i class="fab fa-linkedin-in"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-top ax-section-gap">
            <div class="container">
                <div class="row">
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                        <div class="footer-widget-item moussaid-border-right">
                            <h2>Get in touch!</h2>
                            <p>Subscribe</p>
                            <div class="moussaid-newsletter">
                                <form class="newsletter-form" action="#">
                                    <input type="email" placeholder="Email">
                                    <a class="moussaid-button btn-transparent" href="#"><span
                                            class="button-text">Subscribe</span><span class="button-icon"></span></a>
                                </form>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-3 col-lg-6 col-md-6 col-sm-6 col-12 mt_mobile--30">
                        <div class="footer-widget-item">
                            <h6 class="title">Services</h6>
                            <div class="footer-menu-container">
                                <ul class="ft-menu liststyle link-hover">
                                    <li><a href="{{ route('services.web_development') }}">Web Development</a></li>
                                    <li><a href="{{ route('services.mobile_development') }}">Mobile App Development</a></li>
                                    <li><a href="{{ route('services.uxui_design') }}">UX/UI Design</a></li>
                                    <li><a href="{{ route('services.dedicated_team') }}">Dedicated team</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-2 col-lg-6 col-md-6 col-sm-6 col-12 mt_lg--30 mt_md--30 mt_sm--30">
                        <div class="footer-widget-item">
                            <h6 class="title">Resourses</h6>
                            <div class="footer-menu-container">
                                <ul class="ft-menu liststyle link-hover">
                                    {{-- <li><a href="/blog">Blog</a></li> --}}
                                    <li><a href="{{ route('cases') }}">Case Studies</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-1 col-lg-6 col-md-6 col-sm-6 col-12 mt_lg--30 mt_md--30 mt_sm--30">
                        <div class="footer-widget-item widget-support">
                            <h6 class="title">Support</h6>
                            <div class="footer-menu-container">
                                <ul class="ft-menu liststyle link-hover">
                                    <li><a href="{{ route('contact') }}">Contact</a></li>
                                    <li><a href="#soon">Privacy Policy</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="copyright copyright-default">
            <div class="container">
                <div class="row row--0 ptb--20 moussaid-basic-thine-line">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                        <div class="inner text-center text-md-left">
                            <p>© 2017 - {{ date('Y') }}. All rights reserved. <img src="{{ asset('frontend/assets/images/shape/s1.svg') }}" alt="Almossaid" height="30" /> S.A.M.W.SERVICES LLC </p>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                        <div class="quick-contact">
                            <ul class="link-hover d-flex justify-content-center justify-content-md-end liststyle">
                                <li><a href="#soonnn">Terms of Use</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>



<style>
    @keyframes shadow-pulse {
        0% {
            box-shadow: 0 0 0 0px rgba(0, 0, 0, 0.2);
        }

        100% {
            box-shadow: 0 0 0 35px rgba(0, 0, 0, 0);
        }
    }

    @keyframes shadow-pulse-big {
        0% {
            box-shadow: 0 0 0 0px rgba(0, 0, 0, 0.1);
        }

        100% {
            box-shadow: 0 0 0 70px rgba(0, 0, 0, 0);
        }
    }

    .notif-whatsapp {
        background-color: #1ee62e;

        height: 4rem;
        width: 4rem;
        position: fixed;
        bottom: 3rem;
        left: 3rem;
        border-radius: 50%;
        z-index: 2000;
        box-shadow: 0 1rem 3rem rgba(0, 0, 0, 0.2);
        text-align: center;
        cursor: pointer;
        animation: shadow-pulse 1s infinite;
    }

    .notif-whatsapp .icon {
        height: 4rem;
        width: 4rem;
        line-height: 4rem;
        position: absolute;
        top: .4rem;
        left: 0rem;
        border-radius: 50%;
        z-index: 2000;
        /* box-shadow: 0 1rem 3rem rgba(0,0,0,0.2); */
        text-align: center;
        cursor: pointer;
    }
</style>

<div class="notif-whatsapp">
    <div class="icon">
        <a href="https://api.whatsapp.com/send?phone=+212670039289" target="_blank"><i
                class="fab fa-whatsapp fa-2x" style="color:#fff"></i></a>
    </div>
</div>
