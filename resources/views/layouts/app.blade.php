<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Almossaid - @yield('title' , '') </title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" type="image/x-icon" href="">
    <link rel="icon" type="image/png" href="https://almossaid.com/logo/dark.png" />


    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @yield('meta_tags')

    <link rel="stylesheet" href="{{asset('frontend/assets/css/vendor/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('frontend/assets/css/plugins/slick.css')}}">

    <link rel="stylesheet" href="{{asset('frontend/assets/css/plugins/icomoon.css')}}">
    <link rel="stylesheet" href="{{asset('frontend/assets/css/plugins/plugins.css')}}">
    <link rel="stylesheet" href="{{asset('frontend/assets/css/style.css')}}">

    @yield('css')



    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-747MR74BP3"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'G-747MR74BP3');
    </script>
</head>

<body>
    <div class="main-content">


        @include('/layouts/partials/header')

        <main class="page-wrapper">
            @yield('content')
        </main>


        @include('/layouts/partials/footer')

    </div>


    <!-- Load Facebook SDK for JavaScript -->
    <div id="fb-root"></div>
    <script>
      window.fbAsyncInit = function() {
        FB.init({
          xfbml            : true,
          version          : 'v10.0'
        });
      };

      (function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js';
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>

    <!-- Your Chat Plugin code -->
    <div class="fb-customerchat"
      attribution="setup_tool"
      page_id="755782831198627"
      theme_color="#f9830d">
    </div>

    <script src="{{asset('frontend/assets/js/vendor/modernizr.min.js')}}"></script>
    <script src="{{asset('frontend/assets/js/vendor/jquery.js')}}"></script>
    <script src="https://unpkg.com/swiper/swiper-bundle.min.js')}}"></script>
    <script src="{{asset('frontend/assets/js/vendor/bootstrap.min.js')}}"></script>
    <script src="{{asset('frontend/assets/js/waypoints.min.js')}}"></script>
    <script src="{{asset('frontend/assets/js/wow.js')}}"></script>
    <!-- <script src="assets/js/anime.js"></script> -->
    <script src="{{asset('frontend/assets/js/scrollmagic.js')}}"></script>
    <script src="{{asset('frontend/assets/js/scrollup.js')}}"></script>
    <script src="{{asset('frontend/assets/js/stickysidebar.js')}}"></script>
    <script src="{{asset('frontend/assets/js/main.js')}}"></script>
    <script src="https://use.fontawesome.com/releases/v5.15.1/js/all.js" data-auto-replace-svg="nest"></script>

    @yield('javascript')
</body>

</html>
