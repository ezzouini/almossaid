<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');

Route::get('/', function () {
    return view('pages.home');
})->name('home');

Route::get('/company', function () {
    return view('pages.company');
})->name('company');


Route::group(['as'=>'services.'], function () {

    Route::get('/web-development', function () {
        return view('pages.services.web-development');
    })->name('web_development');

    Route::get('/mobile-development', function () {
        return view('pages.services.mobile-development');
    })->name('mobile_development');

    Route::get('/uxui-design', function () {
        return view('pages.services.uxui-design');
    })->name('uxui_design');

    Route::get('/dedicated-team', function () {
        return view('pages.services.dedicated-team');
    })->name('dedicated_team');

});

Route::get('/expertise', function () {
    return view('pages.expertise');
})->name('expertise');

Route::get('/cases', function () {
    return view('pages.cases');
})->name('cases');

Route::get('/cases/tisaapp', function () {
    return view('pages.cases.tissa');
})->name('cases.tisaapp');


Route::get('/contact', function () {
    return view('pages.contact');
})->name('contact');


Route::group(['prefix' => '/blog'], function () {
    Route::get('/login','Auth\LoginController@showLoginForm')->name('login');
    Route::post('/login','Auth\LoginController@login')->name('login');
    Route::post('/logout','Auth\LoginController@logout')->name('logout');

    Route::get('/','PostController@index')->name('blog');
    Route::get('/post/{slug}','PostController@show_by_slug')->name('post');
    //Route::get('/categories','CategoryController@index')->name('categories');
    Route::get('/category/{slug}','CategoryController@show_by_slug')->name('category');

    Route::get('/author/{username}','AuthorController@show')->name('author');
});


Route::group(['prefix' => '/control','middleware'=>'auth','as'=>'control.','namespace' => 'Control'], function () {

    Route::get('/',function(){ return redirect()->route('control.post'); });
    Route::resource('/post', 'PostController')->names(['index' => 'post','create' => 'post.create', 'store' => 'post.store' ,'show' => 'post.show','update' => 'post.update','destroy' => 'post.destroy' ]);
    Route::resource('/category', 'CategoryController')->names(['index' => 'category','create' => 'category.create', 'store' => 'category.store' ,'show' => 'category.show','update' => 'category.update','destroy' => 'category.destroy' ]);

    Route::get('/profile','ProfileController@index')->name('profile');
    Route::put('/profile','ProfileController@update')->name('profile');
});


Route::group(['prefix' => 'admin','as'=>'admin.','namespace' => 'Admin'], function () {

    Route::get('login/','Auth\loginController@showLoginForm')->name('login');
    Route::post('login/','Auth\loginController@login')->name('login');
    Route::post('logout/','Auth\loginController@logout')->name('logout');

    Route::group(['middleware' => ['auth:admin']], function () {
        Route::get('/','DashboardController@index')->name('dashboard');

        Route::resource('/image', 'ImageController')->names(['index' => 'image','create' => 'image.create', 'store' => 'image.store' ,'show' => 'image.show','update' => 'image.update','destroy' => 'image.destroy' ]);
        Route::resource('/author', 'AuthorController')->names(['index' => 'author','create' => 'author.create', 'store' => 'author.store' ,'show' => 'author.show','update' => 'author.update','destroy' => 'author.destroy' ]);
        Route::resource('/category', 'CategoryController')->names(['index' => 'category','create' => 'category.create', 'store' => 'category.store' ,'show' => 'category.show','update' => 'category.update','destroy' => 'category.destroy' ]);
        Route::resource('/post', 'PostController')->names(['index' => 'post','create' => 'post.create', 'store' => 'post.store' ,'show' => 'post.show','update' => 'post.update','destroy' => 'post.destroy' ]);
    });

});
