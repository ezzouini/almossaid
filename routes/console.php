<?php

use Illuminate\Foundation\Inspiring;
use Illuminate\Support\Facades\Artisan;

use App\Admin;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->describe('Display an inspiring quote');


Artisan::command('admin:add',function(){
    
    $name  = $this->ask('Type your name');
    $email = $this->ask('Type your email address');

    Admin::create([
        'name' => $name,
        'email' => $email,
        'password' => Hash::make('adminalmossaid@112233'),
    ]);

})->describe("Add an administrator to blog"); 